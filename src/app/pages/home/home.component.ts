import { Component } from '@angular/core';
import { LanguageSubscriptionComponent } from 'src/app/shared/models/language-subscription-component';
import { LanguageService } from 'src/app/shared/services/general/language-service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent extends LanguageSubscriptionComponent {
  constructor(languageService: LanguageService) {
    super(languageService);
  }
}
