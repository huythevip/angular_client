import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FindPetsComponent } from 'src/app/components/find-pets/find-pets.component';
import { HomeBodyComponent } from 'src/app/components/home-body/home-body.component';
import { HomePetDetailComponent } from 'src/app/components/home-pet-detail/home-pet-detail.component';
import { MainLoginComponent } from 'src/app/components/main-login/main-login.component';
import { MainRegisterComponent } from 'src/app/components/main-register/main-register.component';
import { GuestOnlyGuard } from 'src/app/shared/guards/guest-only-guard';
import { HomeComponent } from './home.component';

const routes: Routes = [
  { path: '', component: HomeComponent, children: [
    { path: '', component: HomeBodyComponent, },
    {
      path: 'pets',
      children: [
        {
          path: 'find', component: FindPetsComponent,
        },
        {
          path: 'detail/:id', component: HomePetDetailComponent,
        },
      ],
    },
    { path: 'login', component: MainLoginComponent, canActivate: [GuestOnlyGuard] },
    { path: 'register', component: MainRegisterComponent, canActivate: [GuestOnlyGuard] },
  ]},
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule],
})
export class HomeRoutingModule { }
