import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { HomeHeaderComponent } from 'src/app/components/home-header/home-header.component';
import { AntdModule } from 'src/app/shared/modules/antd-modules';
import { HomeBodyComponent } from 'src/app/components/home-body/home-body.component';
import { RegisterModalComponent } from 'src/app/components/register-modal/register-modal.component';
import { MainLoginComponent } from 'src/app/components/main-login/main-login.component';
import { MainRegisterComponent } from 'src/app/components/main-register/main-register.component';
import { ImageSliderComponent } from 'src/app/components/image-slider/image-slider.component';
import { HomePetCardComponent } from 'src/app/components/home-pet-card/home-pet-card.component';
import { FindPetsComponent } from 'src/app/components/find-pets/find-pets.component';
import { HomePetDetailComponent } from 'src/app/components/home-pet-detail/home-pet-detail.component';
import { ScrollTopComponent } from 'src/app/components/scroll-top/scroll-top.component';

@NgModule({
  declarations: [
    HomeComponent,
    ImageSliderComponent,
    HomeHeaderComponent,
    HomeBodyComponent,
    ScrollTopComponent,
    RegisterModalComponent,
    MainLoginComponent,
    HomePetCardComponent,
    FindPetsComponent,
    HomePetDetailComponent,
    MainRegisterComponent,
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    RouterModule,
    AntdModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [
    // ProjectSearchComponent,
    // ServiceProviderSearchComponent,
    HomeHeaderComponent,
  ]
})
export class HomeModule { }
