import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { APP_DEVICES, APP_LANGUAGES } from 'src/app/shared/constants';
import { IUser } from 'src/app/shared/interfaces';
import { LanguageSubscriptionComponent } from 'src/app/shared/models/language-subscription-component';
import { AuthService } from 'src/app/shared/services/general/auth-service';
import { DeviceService } from 'src/app/shared/services/general/device-service';
import { LanguageService } from 'src/app/shared/services/general/language-service';
import { MyAccountOutputService } from './my-account-output-service';

@Component({
  selector: 'app-my-account',
  templateUrl: './my-account.component.html',
  styleUrls: ['./my-account.component.scss'],
  providers: [MyAccountOutputService],
})
export class MyAccountComponent extends LanguageSubscriptionComponent implements OnInit {
  public isDrawerVisible = false;
  public currentDevice: APP_DEVICES;
  public drawerBodyStyle = {
    padding: '0px 20px 0px 0px',
  };

  public currentUser: IUser;

  protected rootPath = '/my-account';

  constructor(
    protected langageService: LanguageService,
    protected deviceService: DeviceService,
    protected authService: AuthService,
    protected outputService: MyAccountOutputService,
    protected router: Router,
  ) {
    super(langageService);
  }

  ngOnInit(): void {
    this.currentUser = this.authService.currentUser;
    if (!this.currentUser) {
      this.router.navigate(["/home"]);

      return;
    }

    this.listenForDeviceChange();
    this.listenForUserChange();
  }

  public toggleDrawer(): void {
    this.isDrawerVisible = !this.isDrawerVisible;
  }

  public get isMobileDevice(): boolean {
    return this.currentDevice === APP_DEVICES.MOBILE;
  }

  public get isUpdatePost(): boolean {
    return this.isCurrentTabActive('/posts/update');
  }

  public isCurrentTabActive(routingPath: string): boolean {
    const matches = this.router.url.match(routingPath);

    return matches && matches.length && true;
    // return `${this.rootPath}${routingPath}` === this.router.url;
  }

  public handleChangeLanguage(lang: string): void {
    return this.outputService.handleChangeLanguageOutput(lang as APP_LANGUAGES);
  }

  public handleSignOut(): void {
    this.outputService.handleSignOutOutput();
  }

  protected listenForDeviceChange(): void {
    this.addSubscription(
      this.deviceService.getDevice$(),
      {
        next: device => this.currentDevice = device,
      }
    );
  }

  protected listenForUserChange(): void {
    this.addSubscription(
      this.authService.getUser$(),
      {
        next: user => {
          if (!user) {
            this.router.navigate(["/home"]);

            return;
          }

          this.currentUser = user;
        },
      }
    );
  }

}
