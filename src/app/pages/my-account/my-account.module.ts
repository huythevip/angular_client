import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyAccountHomeComponent } from 'src/app/components/my-account-home/my-account-home.component';
import { MyAccountDashboardComponent } from 'src/app/components/my-account-dashboard/my-account-dashboard.component';
import { AntdModule } from 'src/app/shared/modules/antd-modules';
import { MyAccountComponent } from './my-account.component';
import { MyAccountRoutingModule } from './my-account-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { UnderConstructionComponent } from 'src/app/components/under-construction/under-construction.component';
import { UserHistoryPostComponent } from 'src/app/components/user-post-history/user-post-history.component';
import { CreatePostComponent } from 'src/app/components/create-post/create-post.component';
import { UserDonationHistoryComponent } from 'src/app/components/user-donation-history/user-donation-history.component';
import { UpdatePostComponent } from 'src/app/components/update-post/update-post.component';
import { UserProfileEditorComponent } from 'src/app/components/user-profile-editor/user-profile-editor.component';

@NgModule({
  declarations: [
    MyAccountComponent,
    MyAccountHomeComponent,
    UnderConstructionComponent,
    MyAccountDashboardComponent,
    UserHistoryPostComponent,
    CreatePostComponent,
    UserProfileEditorComponent,
    UpdatePostComponent,
    UserDonationHistoryComponent,
  ],
  imports: [
    CommonModule,
    MyAccountRoutingModule,
    AntdModule,
    ReactiveFormsModule,
  ]
})
export class MyAccountModule { }
