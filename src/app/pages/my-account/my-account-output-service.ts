import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { APP_LANGUAGES } from 'src/app/shared/constants';
import { IUser } from 'src/app/shared/interfaces';
import { AuthService } from 'src/app/shared/services/general/auth-service';
import { LanguageService } from 'src/app/shared/services/general/language-service';
import { NotificationService } from 'src/app/shared/services/notification/notification-service';

@Injectable()
export class MyAccountOutputService {
  protected user$: Observable<IUser>;

  constructor(
    protected authService: AuthService,
    protected languageService: LanguageService,
    protected router: Router,
    protected notificationService: NotificationService,
  ) {
    this.user$ = authService.getUser$();
  }

  public handleSignOutOutput(): void {
    this.authService.signOut();
    this.notificationService.info(
      this.languageService.renderScript('YOU_LOGGED_OUT_SUCCESSFULLY'),
    );
    this.router.navigate(['/home']);
  }

  public handleSignInOutput(): void {
    return this.authService.loadSampleUser();
  }

  public handleChangeLanguageOutput(lang: APP_LANGUAGES): void {
    return this.languageService.setCurrentLanguage(lang);
  }
}
