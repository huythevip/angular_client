import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreatePostComponent } from 'src/app/components/create-post/create-post.component';
import { FindPetsComponent } from 'src/app/components/find-pets/find-pets.component';
import { MyAccountDashboardComponent } from 'src/app/components/my-account-dashboard/my-account-dashboard.component';
import { UnderConstructionComponent } from 'src/app/components/under-construction/under-construction.component';
import { UpdatePostComponent } from 'src/app/components/update-post/update-post.component';
import { UserDonationHistoryComponent } from 'src/app/components/user-donation-history/user-donation-history.component';
import { UserHistoryPostComponent } from 'src/app/components/user-post-history/user-post-history.component';
import { UserProfileEditorComponent } from 'src/app/components/user-profile-editor/user-profile-editor.component';
import { LoggedUserOnlyGuard } from 'src/app/shared/guards/logged-user-only-guard';
import { MyAccountComponent } from './my-account.component';

const routes: Routes = [
  { path: '', component: MyAccountComponent, canActivate: [LoggedUserOnlyGuard], children: [
    { path: '', component: MyAccountDashboardComponent },
    { path: 'donation-history', component: UserDonationHistoryComponent, },
    {
      path: 'users', children: [
        { path: 'posts', component: UserHistoryPostComponent, },
        { path: 'profile/:userId', component: UserProfileEditorComponent, }
      ]
    },
    {
      path: 'posts',
      children: [
        {
          path: 'create', component: CreatePostComponent,
        },
        {
          path: 'update/:postId', component: UpdatePostComponent,
        },
      ],
    },
    {
      path: 'pets',
      children: [
        {
          path: 'find', component: FindPetsComponent,
        },
      ],
    },
    { path: 'under-construction', component: UnderConstructionComponent },
    { path: '**', redirectTo: '/my-account', },
  ]},
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule],
})
export class MyAccountRoutingModule { }
