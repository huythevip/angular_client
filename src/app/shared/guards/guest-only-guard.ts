import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { AuthService } from '../services/general/auth-service';

@Injectable({ providedIn: 'root' })
export class GuestOnlyGuard implements CanActivate {
  constructor(
    protected authService: AuthService,
    protected router: Router,
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    const isGuest = !this.authService.currentUser;

    if (!isGuest) {
      this.router.navigate(['/']);
    }

    return isGuest;
  }
}
