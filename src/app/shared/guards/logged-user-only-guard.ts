import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { AuthService } from '../services/general/auth-service';

@Injectable({ providedIn: 'root' })
export class LoggedUserOnlyGuard implements CanActivate {
  constructor(
    protected authService: AuthService,
    protected router: Router,
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    const isLoggedUser = this.authService.currentUser;

    if (!isLoggedUser) {
      this.router.navigate(['/home/login']);
    }

    return !!isLoggedUser;
  }
}
