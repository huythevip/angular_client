import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { DataTypes } from '../interfaces';
import { LocalStorageService } from '../services/storage/local-storage-service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(
    protected localStorageService: LocalStorageService,
  ) {}

  public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const authTokenKey = LocalStorageService.KEYS.ACCESS_TOKEN;
    const userAuthToken = this.localStorageService.get(authTokenKey, DataTypes.STRING);
    if (userAuthToken) {
      req.headers.append('Authorization', userAuthToken);
    }

    return next.handle(req);
  }
}
