import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { cloneDeep } from "lodash";
import { Observable, of } from "rxjs";
import { environment } from "src/environments/environment";
import { GeneralTypesDTO } from "../../interfaces";
import { LanguageService } from "../general/language-service";

@Injectable({ providedIn: 'root' })
export class LocationReadService {
  protected apiUrl = `${environment.domains.api}/locations`;

  protected readonly locations: GeneralTypesDTO[] = [
    { id: 1, name: { VIET: 'HCMC', ENG: 'HCMC'}},
    { id: 2, name: { VIET: 'Ha Noi', ENG: 'Hanoi' } },
  ];

  constructor(
    protected httpClient: HttpClient,
    protected languageService: LanguageService,
  ) {}

  public findLocations(options?: any): Observable<GeneralTypesDTO[]> {
    const currentLanguage = this.languageService.currentLanguage;

    return of(cloneDeep(this.locations).map(i => {
      i.name = i.name[currentLanguage];

      return i;
    }));
  }
}