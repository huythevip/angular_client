 import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { cloneDeep } from "lodash";
import { Observable, of } from "rxjs";
import { environment } from "src/environments/environment";
import { GeneralTypesDTO, PaginationOptions } from "../../interfaces";
import { LanguageService } from "../general/language-service";

@Injectable({ providedIn: 'root' })
export class PostReadService {
  protected apiUrl = `${environment.domains.api}/v1`;
  protected allPostTypes: GeneralTypesDTO[] = 
  [
    { id: 1, name: { ENG: 'Search missing', VIET: 'Tìm mất tích' } },
    { id: 2, name: { ENG: 'Adopt', VIET: 'Nhận nuôi' } }
  ];

  constructor(
    protected httpClient: HttpClient,
    protected langageService: LanguageService,
  ) {}

  public findLostPetPosts(options?: PaginationOptions) {}

  public findAdoptablePetPosts(options?: PaginationOptions) {}

  public findPostTypes(options?: any): Observable<GeneralTypesDTO[]> {
    const currentLanguage = this.langageService.currentLanguage;

    return of(cloneDeep(this.allPostTypes).map(i => {
      i.name = i.name[currentLanguage];

      return i;
    }));
  }
}