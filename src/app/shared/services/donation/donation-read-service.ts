import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { UserDonationHistory } from "../../interfaces";

@Injectable({ providedIn: 'root' })
export class DonationReadService {
  protected sampleDonationHistory: UserDonationHistory[] = [
    { id: 1, amount: 20000, date: new Date(), currency: 'VND', message: 'I am a good person!' },
    { id: 2, amount: 20000, date: new Date(), currency: 'VND', message: 'I am a good person!' },
    { id: 3, amount: 5, date: new Date(), currency: 'USD', message: 'I am a good person!' },
    { id: 4, amount: 3, date: new Date(), currency: 'USD', message: 'I am a good person!' },
  ]

  public findUserDonationHistory(userId: number): Observable<UserDonationHistory[]> {
    return new Observable(observer => {
      setTimeout(() => {
        observer.next(this.sampleDonationHistory);
        observer.complete();
      }, 2000);
    });
    // return of(this.sampleDonationHistory);
  }
}