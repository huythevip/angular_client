import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { cloneDeep } from 'lodash';
import { FindPetsDTO, PaginateResult, GeneralTypesDTO, PetDetailDTO, PetBreedDTO } from '../../interfaces';
import { LanguageService } from '../general/language-service';

@Injectable({ providedIn: 'root' })
export class PetReadService {
  protected apiUrl = environment.domains.api;

  protected readonly petTypes: GeneralTypesDTO[] = [
    { id: 1, name: { ENG: 'Dog', VIET: 'Chó' } },
    { id: 2, name: { ENG: 'Cat', VIET: 'Mèo' } },
    { id: 3, name: { ENG: 'Other', VIET: 'Khác' } },
  ];

  protected readonly petSexes: GeneralTypesDTO[] = [
    { id: 1, name: { ENG: 'Male', VIET: 'Đực' } },
    { id: 2, name: { ENG: 'Female', VIET: 'Cái' } },
    { id: 3, name: { ENG: 'Other', VIET: 'Khác' } },
  ];

  protected readonly petAges: GeneralTypesDTO[] = [
    { id: 1, name: { ENG: 'Infant', VIET: 'Con non' } },
    { id: 2, name: { ENG: 'Young', VIET: 'Trẻ con' } },
    { id: 3, name: { ENG: 'Mature', VIET: 'Trưởng thành' } },
    { id: 4, name: { ENG: 'Senior', VIET: 'Lớn tuổi' } },
  ];

  protected readonly petBreeds: PetBreedDTO[] = [
    { id: 1, typeId: 1, name: { ENG: 'Domestic', VIET: 'Chó cỏ' } },
    { id: 2, typeId: 1, name: { ENG: 'Husky', VIET: 'Husky' } },
    { id: 3, typeId: 1, name: { ENG: 'Alaska', VIET: 'Alaska' } },
    { id: 4, typeId: 1, name: { ENG: 'Corgi', VIET: 'Corgi' } },
    { id: 5, typeId: 1, name: { ENG: 'Dachshund', VIET: 'Chó lạp xưởng ' } },
    { id: 6, typeId: 2, name: { ENG: 'Domestic', VIET: 'Mèo mướp' } },
    { id: 7, typeId: 2, name: { ENG: 'Scottish Fold', VIET: 'Anh tai cụp' } },
    { id: 8, typeId: 2, name: { ENG: 'British shorthair', VIET: 'Anh lông ngắn' } },
    { id: 9, typeId: 2, name: { ENG: 'British longhair', VIET: 'Anh lông dài' } },
  ];

  protected readonly petSizes: GeneralTypesDTO[] = [
    { id: 1, name: { ENG: 'Tiny (< 1kg)', VIET: 'Tí hon (< 1kg)' } },
    { id: 2, name: { ENG: 'Small (1-3kg)', VIET: 'Nhỏ (1-3kg)' } },
    { id: 3, name: { ENG: 'Medium (3-5kg)', VIET: 'Trung bình (3-5kg)' } },
    { id: 4, name: { ENG: 'Big (5-10kg)', VIET: 'To (5-10kg)' } },
    { id: 5, name: { ENG: 'Huge (> 10kg)', VIET: 'Khổng lồ (> 10kg)' } },
  ];

  protected readonly petHealthProperties: GeneralTypesDTO[] = [
    { id: 1, name: { ENG: 'Is Vacinated', VIET: 'Đã tiêm phòng' } },
    { id: 2, name: { ENG: 'Is Dewormed', VIET: 'Đã tẩy giun' } },
    { id: 3, name: { ENG: 'Is Sterilized', VIET: 'Đã triệt sản' } },
  ];

  protected readonly samplePetResult: FindPetsDTO[] = [
    { id: 1, typeId: 1, sex: 'Đực', breed: 'Husky', name: 'Test123456', age: 'Con non', size: 'Nhỏ', specialProperties: 'Có sẹo ở mắt phải' },
    { id: 1, typeId: 2, sex: 'Cái', breed: 'Anh Lông Ngắn', name: 'Test123456', age: 'Trưởng thành', size: 'Lớn', specialProperties: 'Có sẹo ở mắt phải' },
    { id: 1, typeId: 1, sex: 'Cái', breed: 'Husky', name: 'Test123456', age: 'Thiếu niên', size: 'Nhỏ', specialProperties: 'Có sẹo ở mắt phải' },
    { id: 1, typeId: 2, sex: 'Cái', breed: 'Anh Lông Ngắn', name: 'Test123456', age: 'Lớn tuổi', size: 'Vừa', specialProperties: 'Có sẹo ở mắt phải' },
    { id: 1, typeId: 1, sex: 'Cái', breed: 'Alaska', name: 'Test123456', age: 'Con non', size: 'Vừa', specialProperties: 'Có sẹo ở mắt phải' },
    { id: 1, typeId: 1, sex: 'Đực', breed: 'Nội địa', name: 'Test123456', age: 'Con non', size: 'Trung bình', specialProperties: 'Có sẹo ở mắt phải' },
    { id: 1, typeId: 2, sex: 'Cái', breed: 'Nội địa', name: 'Test123456', age: 'Trưởng thành', size: 'Trung bình', specialProperties: 'Có sẹo ở mắt phải' },
    { id: 1, typeId: 2, sex: 'Đực', breed: 'Anh Lông Ngắn', name: 'Test123456', age: 'Trưởng thành', size: 'Nhỏ', specialProperties: 'Có sẹo ở mắt phải' },
    { id: 1, typeId: 1, sex: 'Cái', breed: 'Alaska', name: 'Test123456', age: 'Con non', size: 'Vừa', specialProperties: 'Có sẹo ở mắt phải' },
    { id: 1, typeId: 1, sex: 'Đực', breed: 'Nội địa', name: 'Test123456', age: 'Con non', size: 'Trung bình', specialProperties: 'Có sẹo ở mắt phải' },
    { id: 1, typeId: 2, sex: 'Cái', breed: 'Nội địa', name: 'Test123456', age: 'Trưởng thành', size: 'Trung bình', specialProperties: 'Có sẹo ở mắt phải' },
    { id: 1, typeId: 2, sex: 'Đực', breed: 'Anh Lông Ngắn', name: 'Test123456', age: 'Trưởng thành', size: 'Nhỏ', specialProperties: 'Có sẹo ở mắt phải' },
  ];

  constructor(
    protected httpClient: HttpClient,
    protected languageService: LanguageService,
  ) {}

  public findPets(options: any): Observable<PaginateResult<FindPetsDTO>> {
    return of({
      docs: this.samplePetResult,
      totalCount: 150,
    });
  }

  public findPetTypes(): Observable<GeneralTypesDTO[]> {
    const currentLanguage = this.languageService.currentLanguage;

    return of(cloneDeep(this.petTypes).map(i => {
      i.name = i.name[currentLanguage];

      return i;
    }));
  }

  public findPetHealthProperties(): Observable<GeneralTypesDTO[]> {
    const currentLanguage = this.languageService.currentLanguage;

    return of(cloneDeep(this.petHealthProperties).map(i => {
      i.name = i.name[currentLanguage];

      return i;
    }));
  }

  public findPetSizes(): Observable<GeneralTypesDTO[]> {
    const currentLanguage = this.languageService.currentLanguage;

    return of(cloneDeep(this.petSizes).map(i => {
      i.name = i.name[currentLanguage];

      return i;
    }));
  }

  public findPetBreeds(): Observable<PetBreedDTO[]> {
    const currentLanguage = this.languageService.currentLanguage;

    return of(cloneDeep(this.petBreeds).map(i => {
      i.name = i.name[currentLanguage];

      return i;
    }));
  }

  public findPetAges(): Observable<GeneralTypesDTO[]> {
    const currentLanguage = this.languageService.currentLanguage;

    return of(cloneDeep(this.petAges).map(i => {
      i.name = i.name[currentLanguage];

      return i;
    }));
  }

  public findPetDetailById(id: number): Observable<PetDetailDTO> {
    return of({
      id: 1,
      age: 'Con non',
      imageUrls: [
        '/assets/images/sample_pet_carousel_1.jpg',
        '/assets/images/sample_pet_carousel_2.jpg',
        '/assets/images/sample_pet_carousel_3.jpg',
        '/assets/images/sample_pet_carousel_1.jpg',
        '/assets/images/sample_pet_carousel_2.jpg',
        '/assets/images/sample_pet_carousel_3.jpg',
        '/assets/images/sample_pet_carousel_1.jpg',
        '/assets/images/sample_pet_carousel_2.jpg',
        '/assets/images/sample_pet_carousel_3.jpg',
      ],
      breed: 'Chó ta',
      location: 'Quận 1, Tp. HCM',
      name: 'Bông mập',
      sex: 'Đực',
      isAdopt: true,
      isDone: true,
      size: 'Nhỏ',
      type: 'Chó',
      specialProperties: [
        {
          id: 1,
          value: false,
        },
        {
          id: 2,
          value: true,
        },
        {
          id: 3,
          value: true,
        },
      ],
      story: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book`,
      contact: {
        id: 10,
        name: 'Trần Quang Huy',
        location: 'Quân Bình Tân, TP. HCM',
        phone: '0931111111',
        email: 'huy.tq2106@gmail.com',
      },
    });
  }

  public findPetSexes(): Observable<GeneralTypesDTO[]> {
    const currentLanguage = this.languageService.currentLanguage;
    
    return of(cloneDeep(this.petSexes).map(i => {
      i.name = i.name[currentLanguage];

      return i;
    }));
  }
}
