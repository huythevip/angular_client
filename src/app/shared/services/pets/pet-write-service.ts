import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { MutationResponse } from '../../interfaces';

@Injectable({ providedIn: 'root' })
export class PetWriteService {
  constructor() {}

  public updatePetById(petId: number, payload: any): Observable<MutationResponse> {
    return new Observable(observer => {
      setTimeout(() => {
        observer.next({ success: true, });
        observer.complete();
      }, 2000);
    });
  }
}
