import { Injectable } from '@angular/core';
import { NzNotificationDataOptions, NzNotificationService } from 'ng-zorro-antd/notification';

@Injectable({ providedIn: 'root' })
export class NotificationService {
  protected defaultOptions: NzNotificationDataOptions = {
    nzDuration: 3000,
  };

  constructor(
    protected notification: NzNotificationService,
  ) {}

  public success(content: string, title = 'Success', options = this.defaultOptions): void {
    this.notification.success(title, content, options);
  }

  public info(content: string, title = 'Info', options = this.defaultOptions): void {
    this.notification.info(title, content, options);
  }

  public error(content: string, title = 'Error', options = this.defaultOptions): void {
    this.notification.error(title, content, options);
  }
}
