import { Injectable } from '@angular/core';
import { DataTypes } from '../../interfaces';

@Injectable({ providedIn: 'root' })
export class LocalStorageService {
  public static readonly KEYS = {
    ACCESS_TOKEN: 'access_token',
  }

  public get(key: string, dataType: DataTypes): string {
    try {
      const item = localStorage.getItem(key);

      switch (dataType) {
        case DataTypes.OBJECT:
          return JSON.parse(item);
        case DataTypes.STRING:
          return item;
      }
    } catch (error) {
      throw new Error(`Error getting item ${key} from local storage`);
    }
  }

  public set(key: string, data: string): void {
    localStorage.setItem(key, data);

    return;
  }

  public remove(key: string): void {
    localStorage.removeItem(key);
  }
}
