import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { GeneralTypesDTO, IUser, PaginateResult, PaginationOptions, UserHistoryPost } from '../../interfaces';
import { LanguageService } from '../general/language-service';

@Injectable({ providedIn: 'root' })
export class UserReadService {
  protected apiUrl = `${environment.domains.api}/users`;

  constructor(
    protected httpClient: HttpClient,
    protected languageService: LanguageService,
  ) {}

  public getUser(data: any): Observable<any> {
    return of([]);
  }

  public findUserTypes(): Observable<GeneralTypesDTO[]> {
    const currentLanguage = this.languageService.currentLanguage;

    return of([
      { id: 1, name: { ENG: 'Administrator', VIET: 'Quản trị viên' }, },
      { id: 2, name: { ENG: 'Moderator', VIET: 'Điều hành viên' }, },
      { id: 3, name: { ENG: 'Member', VIET: 'Thành viên' }, },
      { id: 4, name: { ENG: 'Silver Member', VIET: 'Thành viên bạc' }, },
      { id: 5, name: { ENG: 'Gold Member', VIET: 'Thành viên vàng' }, },
      { id: 6, name: { ENG: 'Platinum Member', VIET: 'Thành viên bạch kim' }, },
      { id: 7, name: { ENG: 'Diamong Member', VIET: 'Thành viên kim cương' }, },
    ].map(i => ({...i, name: i.name[currentLanguage]})));
  }

  public findUserPosts(userId: number, options?: PaginationOptions): Observable<PaginateResult<UserHistoryPost>> {
    const observable = new Observable<PaginateResult<UserHistoryPost>>(observer => {
      setTimeout(() => {
        observer.next({
          docs: [
            { id: 1, petType: 'Dog', postType: 'Search missing', title: 'Lorem ipsum', postDate: new Date().toISOString() },
            { id: 2, petType: 'Dog', postType: 'Adopt', title: 'Lorem ipsum', postDate: new Date().toISOString() },
            { id: 3, petType: 'Cat', postType: 'Search missing', title: 'Lorem ipsum', postDate: new Date().toISOString() },
            { id: 4, petType: 'Cat', postType: 'Adopt', title: 'Lorem ipsum', postDate: new Date().toISOString() },
            { id: 5, petType: 'Dog', postType: 'Search missing', title: 'Lorem ipsum', postDate: new Date().toISOString() },
          ],
          totalCount: 10,
        });

        observer.complete();
        observer.unsubscribe();
      }, 1000);
    });
    
    return observable;
  }

  public findUserById(userId: number): Observable<IUser> {
    const observable = new Observable<IUser>(observer => {
      setTimeout(() => {
        observer.next({
          id: 1,
          fullName: 'Tran Quang Huy',
          status: 1,
          userTypeId: 1,
          address: 'Tp.HCM',
          email: 'huy.tq2106@gmail.com',
          phoneNumber: '097777777',
          avatarUrl: '',
          bio: 'I am just a pet lover',
        });

        observer.complete();
        observer.unsubscribe();
      }, 1000);
    });
    
    return observable;
  }

  public findUserPostById(userId: number, postId: number): Observable<any> {
    const observable = new Observable<any>(observer => {
      setTimeout(() => {
        observer.next({
          id: 1,
          generalInfo: {
            petName: 'Lulu',
            title: 'Mèo ngoan thất lạc', 
            ageId: 1,
            postTypeId: 1,
            typeId: 2,
            sexId: 1,
            breedId: 2,
            sizeId: 3,
            specialProperties: 'Có vòng cổ lục lạc màu xám',
          },
          imageUrls: [],
          healthProperties: {
            isVacinated: true,
            isDewormed: true,
            isSterilized: false,
          },
          contact: {
            name: 'Trần Quang Huy',
            address: 'Tp.HCM',
            phone: '093777777',
          },
          createdAt: new Date().toISOString(),
        });

        observer.complete();
        observer.unsubscribe();
      }, 1000);
    });
    
    return observable;
  }
}
