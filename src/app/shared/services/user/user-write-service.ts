import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { GeneralSuccessModifyingResponse, IUser } from '../../interfaces';

@Injectable({ providedIn: 'root' })
export class UserWriteService {
  protected apiUrl = `${environment.domains.api}/users`;

  constructor(
    protected httpClient: HttpClient,
  ) {}

  public createUser(user: Partial<IUser>): Observable<IUser> {
    return this.httpClient.post<IUser>(this.apiUrl, user);
  }

  public updateUserById(userId: number, updateData: Partial<IUser>): Observable<GeneralSuccessModifyingResponse> {
    return new Observable(observer => {
      setTimeout(() => {
        observer.next({ success: true });
        observer.complete();
      }, 500);
    })
  }
}
