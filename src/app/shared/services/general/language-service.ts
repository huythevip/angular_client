import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { APP_LANGUAGES } from '../../constants';
import LanguageScripts from 'src/app/shared/texts';

@Injectable({ providedIn: 'root' })
export class LanguageService {
  protected languagePublisher = new BehaviorSubject<APP_LANGUAGES>(APP_LANGUAGES.VIET);

  public setCurrentLanguage(lang: APP_LANGUAGES): void {
    this.languagePublisher.next(lang);
  }

  public renderScript(scriptName: string, ...args: any[]): string {
    return LanguageScripts[this.currentLanguage][scriptName](...args);
  }

  public getLanguage$(): Observable<APP_LANGUAGES> {
    return this.languagePublisher.asObservable();
  }

  public get currentLanguage(): APP_LANGUAGES {
    return this.languagePublisher.value;
  }
}
