import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

export interface UploadResult {
  isSuccess: boolean;
  message?: string;
  fileUrl?: string;
}

export interface UploadUrlRequestResult {
  isSuccess: boolean;
  message?: string;
  uploadUrl?: string;
}

@Injectable({ providedIn: 'root' })
export class UploadService {
  public upload(url: string, file: File, options?: any): Observable<UploadResult> {
    return new Observable<UploadResult>(observer => {
      setTimeout(() => {
        observer.next({
          isSuccess: true,
          fileUrl: 'assets/images/pet_adopt.png',
        });
      }, 1500);
    });
  }

  public getUploadUrl(options?: any): Observable<UploadUrlRequestResult> {
    return new Observable<UploadUrlRequestResult>(observer => {
      setTimeout(() => {
        observer.next({
          isSuccess: true,
          uploadUrl: 'http://localhost:3005/upload',
        });
      }, 1500);
    });
  }
}