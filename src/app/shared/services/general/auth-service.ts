import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { LocalStorageService } from '../storage/local-storage-service';
import { AuthCredentials, DataTypes, IUser } from '../../interfaces';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

const sampleUser: IUser = {
  id: 1,
  email: 'huy.tq2106@gmail.com',
  avatarUrl: '',
  fullName: 'Tran Quang Huy',
  status: 1,
  userTypeId: 1,
  address: 'Tp.HCM',
  phoneNumber: '093777777',
  bio: 'I am a pet lover!',
  createdAt: new Date(),
  updatedAt: new Date(),
};

@Injectable({ providedIn: 'root' })
export class AuthService {
  protected authUrl = environment.domains.auth;

  constructor(
    protected localStorageService: LocalStorageService,
    protected httpClient: HttpClient,
  ) {}

  protected userProviderSource: BehaviorSubject<IUser> = new BehaviorSubject(sampleUser);

  public getUser$(): Observable<IUser> {
    return this.userProviderSource.asObservable();
  }

  public loadSampleUser(): void {
    this.userProviderSource.next(sampleUser);
  }

  public signIn(credentials: AuthCredentials): Observable<IUser> {
    this.userProviderSource.next(sampleUser);

    return of(sampleUser);
  }

  public get currentUser(): IUser {
    return this.userProviderSource.value;
  }

  public getCurrentUserToken(): string {
    const storageKey = LocalStorageService.KEYS.ACCESS_TOKEN;
    return this.localStorageService.get(storageKey, DataTypes.STRING);
  }

  public signOut(): void {
    this.userProviderSource.next(null);
  }
}
