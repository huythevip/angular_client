import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { APP_DEVICES } from '../../constants';

@Injectable({ providedIn: 'root' })
export class DeviceService {
  protected devicePublisher = new BehaviorSubject<APP_DEVICES>(null);

  constructor() {
    this.initializeDeviceType();
    window.addEventListener('resize', (event) => this.listenForScreenSizeChange(event));
  }

  public getDevice$(): Observable<APP_DEVICES> {
    return this.devicePublisher.asObservable();
  }

  protected getScreenWidth(window: Window): number {
    return window.innerWidth < window.outerWidth ? window.innerWidth : window.outerWidth;
  }

  protected initializeDeviceType(): void {
    const screenWidth = this.getScreenWidth(window);
    const initialDevice = this.getDeviceTypeFromWidth(screenWidth);

    this.devicePublisher.next(initialDevice);
  }

  protected listenForScreenSizeChange(event): void {
    const newWidth = this.getScreenWidth(event.target as Window);
    const newDeviceType = this.getDeviceTypeFromWidth(newWidth);

    if (newDeviceType === this.devicePublisher.value) {
      return;
    }

    this.devicePublisher.next(newDeviceType);
  }

  protected getDeviceTypeFromWidth(screenWidth: number): APP_DEVICES {
    const isDesktop = screenWidth > 959;
    const isTablet = screenWidth > 600 && screenWidth <= 959;
    const isMobile = screenWidth <= 600;

    if (isDesktop) { return APP_DEVICES.DESKTOP; }
    if (isTablet) { return APP_DEVICES.TABLET; }
    if (isMobile) { return APP_DEVICES.MOBILE; }
  }
}
