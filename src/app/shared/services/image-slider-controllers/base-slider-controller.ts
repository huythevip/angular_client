import { Observable, Subject } from "rxjs";
import { ImageSliderNavigationEvents } from "../../constants";
export class ImageSliderController {
  protected scrollEventEmitter = new Subject<ImageSliderNavigationEvents>();

  public get navigation$(): Observable<ImageSliderNavigationEvents> {
    return this.scrollEventEmitter.asObservable();
  }

  public scrollRight(): void {
    this.scrollEventEmitter.next(ImageSliderNavigationEvents.FORWARD);
  }

  public scrollLeft(): void {
    this.scrollEventEmitter.next(ImageSliderNavigationEvents.BACK);
  }
}