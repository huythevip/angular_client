import { Injectable } from "@angular/core";
import { ImageSliderController } from "./base-slider-controller";

@Injectable()
export class TestimonialSliderController extends ImageSliderController {}