import { Injectable, OnDestroy } from '@angular/core';
import { v4 } from 'uuid';
import { CallBackFn, RegisteredVisibilityElement } from '../interfaces';

@Injectable()
export class ElementVisibilityHelper {
  protected isRegisteredInParentOnDestroy = false;
  protected registeredElementsMap: Map<string, RegisteredVisibilityElement> = new Map();

  public listenForElementVisibilityEvents(element: Element, showUpCallback: CallBackFn, disapearCallback?: CallBackFn): string {
    const elementId = v4();
    this.registeredElementsMap.set(elementId, {
      element,
      onShowCallback: showUpCallback,
      onDisapearCallback: disapearCallback,
      isShownUp: false,
    });

    return elementId;
  }

  public removeElementVisibilityListener(id: string): void {
    this.registeredElementsMap.delete(id);
  }

  public startListenForAllRegisterElements(): void {
    if (!this.isRegisteredInParentOnDestroy) {
      throw new Error('Must call clean up method first!');
    }

    document.addEventListener('scroll', this.handleViewportChange.bind(this));
    document.addEventListener('resize', this.handleViewportChange.bind(this));
    document.addEventListener('load', this.handleViewportChange.bind(this));
  }

  public cleanUpWhenParentDestroys(parentComponent: OnDestroy): void {
    const originalParentDestroyFn = parentComponent.ngOnDestroy.bind(parentComponent);

    parentComponent.ngOnDestroy = () => {
      document.removeEventListener('scroll', this.handleViewportChange.bind(this));
      document.removeEventListener('resize', this.handleViewportChange.bind(this));
      document.removeEventListener('load', this.handleViewportChange.bind(this));

      this.registeredElementsMap.clear();
      originalParentDestroyFn();
    };

    this.isRegisteredInParentOnDestroy = true;
  }

  protected handleViewportChange(): void {
    this.allRegisteredElements.forEach(registeredElement => {
      if (this.isElementShowUp(registeredElement.element)) {
        if (registeredElement.isShownUp) {
          return;
        }

        registeredElement.onShowCallback();
        registeredElement.isShownUp = true;
      } else if (registeredElement.isShownUp === true && registeredElement.onDisapearCallback) {
        registeredElement.onDisapearCallback();
      }
    });
  }

  protected get allRegisteredElements(): RegisteredVisibilityElement[] {
    return Array.from(this.registeredElementsMap.values());
  }

  protected isElementShowUp(element: Element): boolean {
    const domRect = element.getBoundingClientRect();

    return domRect.y  - window.innerHeight <= 0;
  }
}
