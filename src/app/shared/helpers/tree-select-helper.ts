import { Injectable } from '@angular/core';
import { HasChildrenIds, TreeSelectNode } from '../interfaces';

@Injectable()
export class TreeSelectHelper {
  protected allItemsTreeMap: Map<any, HasChildrenIds> = new Map();
  protected allParentItemsTreeMap: Map<any, HasChildrenIds> = new Map();

  public normalizeObjectToTreeNode(data: HasChildrenIds[], titlePropName: string, keyPropName: string): TreeSelectNode[] {
    if (!data || !data.length) {
      throw new Error('Missing data to create tree');
    }

    data.forEach(item => {
      const isParent = this.isParentItem(item);
      if (isParent) {
        this.allParentItemsTreeMap.set(item[keyPropName], item);
      }

      this.allItemsTreeMap.set(item[keyPropName], item);
    });

    return this.allParentIdsList.map(item => this.processTreeItem(item, titlePropName, keyPropName));
  }

  protected processTreeItem(itemId: number, titlePropName: string, keyPropName: string): TreeSelectNode {
    const item = this.allItemsTreeMap.get(itemId);

    const isParent = this.isParentItem(item);
    const title = item[titlePropName];
    const key = item[keyPropName];
    const value = key;

    let children: TreeSelectNode[] = [];

    if (isParent) {
      children = item.childrenIds.map(childItemId => this.processTreeItem(childItemId , titlePropName, keyPropName));

      return {
        key,
        title,
        value,
        children
      };
    }

    return {
      isLeaf: true,
      key,
      title,
      value,
    };
  }

  protected get allParentIdsList(): number[] {
    return Array.from(this.allParentItemsTreeMap.keys());
  }

  protected isParentItem(item: HasChildrenIds): boolean {
    return item.childrenIds && item.childrenIds.length && true;
  }
}
