import { Injectable } from "@angular/core";
import { AbstractControl } from "@angular/forms";
import { APP_LANGUAGES } from "../constants";
import { LanguageService } from "../services/general/language-service";

@Injectable({ providedIn: 'root' })
export class ErrorTipMessageHelper {
  constructor(
    protected languageService: LanguageService,
  ) {}

  public getErrorMessageForControl(formControl: AbstractControl, allErrors = false): string {
    const { valid, errors: validationErrors } = formControl;
    if (valid || !validationErrors) {
      return;
    }

    let errorMessages: string[] = [];

    if (validationErrors.required) {
      errorMessages.push(this.languageService.renderScript('INPUT_REQUIRED'));
    }

    if (validationErrors.email) {
      errorMessages.push(this.languageService.renderScript('INVALID_EMAIL'));
    }

    if (validationErrors.minlength) {
      const { requiredLength } = validationErrors.minlength;

      errorMessages.push(this.languageService.renderScript('MIN_LENGTH_ERROR', requiredLength));
    }

    if (validationErrors.maxlength) {
      const { requiredLength } = validationErrors.maxlength;

      errorMessages.push(this.languageService.renderScript('MAX_LENGTH_ERROR', requiredLength));
    }

    if (validationErrors.min) {
      errorMessages.push(this.languageService.renderScript('MINIMUM_ERROR'));
    }

    if (validationErrors.max) {
      errorMessages.push(this.languageService.renderScript('MAXIMUM_ERROR'));
    }

    if (allErrors) {
      return errorMessages.join('. ');
    }

    return errorMessages[0];

  }
}