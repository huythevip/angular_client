import { HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { LanguageService } from "../services/general/language-service";
import { NotificationService } from "../services/notification/notification-service";
import scripts from 'src/app/shared/texts';
import { of } from "rxjs";

@Injectable({ providedIn: 'root' })
export class HttpErrorHandlerPipe {
  constructor(
    protected notificationService: NotificationService,
    protected languageService: LanguageService,
  ) {}

  protected get currentLanguage(): string {
    return this.languageService.currentLanguage;
  }

  public defaultHandler(returnValue?: any, onError?: (error: HttpErrorResponse) => void) {
    return (error: HttpErrorResponse) => {
      this.notificationService.error(
        scripts[this.currentLanguage].ERROR(),
        scripts[this.currentLanguage].DEFAULT_ERROR_MESSAGE(),
      );

      if (onError) {
        onError(error);
      }

      return of(returnValue || []);
    }
  }
}