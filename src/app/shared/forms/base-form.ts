import { AbstractControl, FormGroup } from '@angular/forms';
export abstract class BaseForm<T = any> {
  public static FORM_CONTROLS: any;
  protected form: FormGroup;

  constructor() {
    this.initForm();
  }

  public get formGroup(): FormGroup {
    return this.form;
  }

  public get formValue(): T {
    return this.form.value;
  }

  public getSubFormGroup(formGroupName: string): AbstractControl {
    return this.form.get(formGroupName);
  }

  public getFormControl(name: string): AbstractControl {
    const control = this.form.get(name);
    if (!control) {
      throw new Error(`Form Control name ${control} not exist`);
    }

    return control;
  }

  public toggleFormControlDisability(formControlName: string, forceDisabled = false): void {
    const formControl = this.getFormControl(formControlName);

    if (forceDisabled) {
      formControl.disable();

      return;
    }

    const currentDisabilityValue = formControl.disabled;

    if (currentDisabilityValue === true) {
      formControl.enable();
    } else {
      formControl.disable();
    }
  }

  public disableFormControl(formControlName: string): void {
    this.getFormControl(formControlName).disable();
  }

  public disableAllFormControls(): void {
    for (const formControl in this.formGroup.controls) {
      this.toggleFormControlDisability(formControl, true);
    }
  }

  public isControlValid(controlName: string): boolean {
    return this.getFormControl(controlName).valid;
  }

  protected get isFormValid(): boolean {
    return this.form.valid;
  }

  protected abstract initForm(): void;
}
