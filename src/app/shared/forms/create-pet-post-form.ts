import { Injectable } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { BaseForm } from "./base-form";

@Injectable()
export class CreatePetPostForm extends BaseForm {
  constructor() {
    super();
  }

  protected initForm() {
    this.form = new FormGroup({
      generalInfo: new FormGroup({
        title: new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(255)]),
        petName: new FormControl('', [Validators.minLength(2), Validators.maxLength(50)]),
        ageId: new FormControl(null, [Validators.required]),
        postTypeId: new FormControl(1, [Validators.required]),
        typeId: new FormControl(1),
        sexId: new FormControl(null, [Validators.required]),
        breedId: new FormControl(null, [Validators.required]),
        sizeId: new FormControl(null, [Validators.required]),
        specialProperties: new FormControl(''),
      }),
      imageUrls: new FormControl([]),
      healthProperties: new FormGroup({
        isVacinated: new FormControl(false),
        isDewormed: new FormControl(false),
        isSterilized: new FormControl(false),
      }),
      contact: new FormGroup({
        name: new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(100)]),
        phone: new FormControl('', [Validators.required, Validators.minLength(9), Validators.maxLength(20)]),
        address: new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(100)]),
      }),
    });
  }
}
