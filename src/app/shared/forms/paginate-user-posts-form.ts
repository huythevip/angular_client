import { Injectable } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { UserReadService } from "../services/user/user-read-service";
import { BaseForm } from "./base-form";

@Injectable()
export class PaginateUserPostsForm extends BaseForm {
  constructor(
    protected userReadService: UserReadService,
  ) {
    super();
  }

  protected initForm() {
    this.form = new FormGroup({
      limit: new FormControl(10),
      offset: new FormControl(0),
      postType: new FormControl(),
      petType: new FormControl(),
      title: new FormControl(null, [ Validators.minLength(3), Validators.maxLength(30) ]),
    });
  }
}