import { Injectable } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { catchError } from 'rxjs/operators';
import { UserReadService } from '../services/user/user-read-service';
import { BaseForm } from './base-form';
import { Observable } from 'rxjs';
import { IUser } from '../interfaces';

@Injectable()
export class LoginForm extends BaseForm {
  constructor(
    protected userReadService: UserReadService,
  ) {
    super();
  }

  protected initForm(): void {
    this.form = new FormGroup({
      email: new FormControl('', [
        Validators.required,
        Validators.email,
      ]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(50),
      ]),
      isKeepLoggedIn: new FormControl(false),
    });
  }

  public submit(): Observable<IUser> {
    const isFormValid = this.form.valid;

    if (!isFormValid) {
      throw new Error('Login form not valid');
    }

    return this.userReadService.getUser(this.form.value).pipe(
      catchError((error, user) => {

        return user;
      }),
    );
  }
}
