import { Injectable } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { BaseForm } from "./base-form";

@Injectable()
export class PaginatePetsForm extends BaseForm {
  constructor() {
    super();
  }

  protected initForm() {
    this.form = new FormGroup({
      limit: new FormControl(10),
      offset: new FormControl(0),
      purpose: new FormControl('findLost'),
      typeId: new FormControl(null),
      minPostDate: new FormControl(null),
      locationId: new FormControl(null),
      breedId: new FormControl(null),
      sexId: new FormControl(null),
      ageId: new FormControl(null),
      sizeId: new FormControl(null),
      specialProperties: new FormControl(null),
    });
  }
}