import { Injectable } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BaseForm } from './base-form';

@Injectable()
export class UpdateUserProfileForm extends BaseForm {
  protected initForm(): void {
    this.form = new FormGroup({
      email: new FormControl('', [
        Validators.required,
        Validators.email,
      ]),
      fullName: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(50),
      ]),
      phoneNumber: new FormControl('', [
        Validators.required,
        Validators.minLength(7),
        Validators.maxLength(20),
      ]),
      status: new FormControl(null, [
        Validators.required,
        Validators.min(1),
        Validators.max(3),
      ]),
      address: new FormControl('', [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(255),
      ]),
      userTypeId: new FormControl(null, [
        Validators.required,
        Validators.min(1),
        Validators.max(10),
      ]),
      avatarUrl: new FormControl(''),
      bio: new FormControl('', [
        Validators.minLength(6),
        Validators.maxLength(500),
      ])
    });
  }
}
