import { Injectable } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { BaseForm } from "./base-form";

@Injectable()
export class UserSecurityUpdateForm extends BaseForm {
  protected initForm(): void {
    this.form = new FormGroup({
      userTypeId: new FormControl(null, [
        Validators.required,
        Validators.min(1),
        Validators.max(10),
      ]),
      currentPassword: new FormControl('', [
        Validators.minLength(6),
        Validators.maxLength(50),
      ]),
      newPassword: new FormControl('', [
        Validators.minLength(6),
        Validators.maxLength(50),
      ]),
      confirmPassword: new FormControl('', [
        Validators.minLength(6),
        Validators.maxLength(50),
      ]),
    })
  }
}