import { Injectable } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { IUser } from '../interfaces';
import { UserWriteService } from '../services/user/user-write-service';
import { BaseForm } from './base-form';

@Injectable()
export class RegisterForm extends BaseForm {
  constructor(
    protected userWriteService: UserWriteService,
  ) {
    super();
  }

  public submit(): Observable<IUser> {
    const isFormValid = this.form.valid;

    if (!isFormValid) {
      throw new Error('Register form not valid');
    }

    return this.userWriteService.createUser(this.form.value).pipe(
      catchError((error, user) => {
        console.log(error);

        return user;
      }),
    );
  }

  protected get isFormValid(): boolean {
    const isFormValid = this.form.valid;

    if (!isFormValid) {
      return false;
    }

    return this.getFormControl('password').value === this.getFormControl('confirmPassword').value;
  }

  protected initForm(): void {
    this.form = new FormGroup({
      email: new FormControl('', [
        Validators.required,
        Validators.email,
      ]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(50),
      ]),
      passwordConfirmation: new FormControl('', [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(50),
      ]),
      fullName: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(50),
      ]),
      avatarUrl: new FormControl(''),
    });
  }

}
