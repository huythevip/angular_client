export interface FetchAllOptions {
  withUserToken?: boolean;
}

export enum DataTypes {
  OBJECT = 'object',
  STRING = 'string',
}

export interface MutationResponse {
  success: boolean;
  message?: string;
}

export interface IUser {
  id: number;
  fullName: string;
  email: string;
  status: number;
  userTypeId: number;
  phoneNumber: string;
  address: string;
  createdAt?: Date;
  updatedAt?: Date;
  avatarUrl?: string;
  bio?: string;
}

export interface GeneralSuccessModifyingResponse {
  success: true;
}

export interface UserDonationHistory {
  id: number;
  amount: number;
  currency: string;
  date: Date;
  message: string;
}

export interface PaginateResult<T> {
  docs: T[];
  totalCount: number;
}

export interface PetDetailDTO {
  id: number;
  name?: string;
  isAdopt?: boolean;
  isDone?: boolean;
  imageUrls: string[];
  location: string;
  type: string;
  sex: string;
  age: string;
  breed: string;
  size: string;
  story?: string;
  specialProperties?: object;
  contact: {
    id: number;
    name: string;
    phone: string;
    email: string;
    location: string;
  }
}

export interface UserHistoryPost {
  id: number;
  postType: string;
  postDate: string;
  petType: string;
  title: string;
}

export interface GeneralTypesDTO {
  id: number;
  name: { [language: string]: any };
}

export interface PetBreedDTO extends GeneralTypesDTO {
  typeId: number;
}

export interface PaginationOptions {
  limit: number;
  offset: number;
  filters: IFilter[];
  sort: ISort;
}

export interface IFilter {
  code: string;
  operator: string;
  value: any;
}

export interface ISort {
  column: string;
  direction: 'asc' | 'desc'
}

export interface FindPetsDTO {
  id: number;
  imgUrl?: string;
  name: string;
  typeId: number;
  sex: string;
  age: string;
  breed: string;
  size: string;
  specialProperties: string;
}
export interface AuthCredentials {
  email: string;
  password: string;
}

export interface RegisteredVisibilityElement {
  element: Element;
  isShownUp: boolean;
  onShowCallback: CallBackFn;
  onDisapearCallback?: CallBackFn;
}

export type CallBackFn = (...args: any[]) => any;

export interface TreeSelectNode {
  title: string;
  key: string | number;
  value: string | number;
  children?: TreeSelectNode[];
  isLeaf?: boolean;
}

export interface HasChildrenIds {
  childrenIds: any[];
}
