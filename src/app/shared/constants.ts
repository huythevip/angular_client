export enum APP_DEVICES {
  DESKTOP = 'DESKTOP',
  TABLET = 'TABLET',
  MOBILE = 'MOBILE',
}

export enum KEYCODES {
  ENTER = 13,
  BACKSPACE = 8,
}

export enum USER_TYPES {
  ADMINISTRATOR = 1,
  MODERATOR,
  USER
}

export enum ImageSliderMode {
  CLICK = 'CLICK',
  DRAG = 'DRAG',
}

export enum ImageSliderNavigationEvents {
  FORWARD = 'FORWARD',
  BACK = 'BACK',
}

export enum APP_LANGUAGES {
  ENG = 'ENG',
  VIET = 'VIET',
  CHINESE = 'CHINESE',
}
