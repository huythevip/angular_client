import { NgModule } from '@angular/core';
import { NzDrawerModule } from 'ng-zorro-antd/drawer';
import { IconsProviderModule } from 'src/app/icons-provider.module';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzCarouselModule } from 'ng-zorro-antd/carousel';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzRateModule } from 'ng-zorro-antd/rate';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NzStepsModule } from 'ng-zorro-antd/steps';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzTreeSelectModule } from 'ng-zorro-antd/tree-select';
import { NzCascaderModule } from 'ng-zorro-antd/cascader';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { NzAutocompleteModule } from 'ng-zorro-antd/auto-complete';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { NzBackTopModule } from 'ng-zorro-antd/back-top';


const antdModules = [
  NzDrawerModule,
  IconsProviderModule,
  NzButtonModule,
  NzMenuModule,
  NzLayoutModule,
  NzCarouselModule,
  NzDropDownModule,
  NzBackTopModule,
  NzPaginationModule,
  NzCascaderModule,
  NzRateModule,
  NzTagModule,
  NzSwitchModule,
  NzModalModule,
  NzDividerModule,
  NzTableModule,
  NzFormModule,
  NzInputModule,
  NzCheckboxModule,
  NzStepsModule,
  NzTabsModule,
  NzTreeSelectModule,
  NzSelectModule,
  NzAutocompleteModule,
  NzDatePickerModule,
  NzUploadModule,
];


@NgModule({
  imports: antdModules,
  exports: antdModules,
})
export class AntdModule {}
