import { Component, OnDestroy } from '@angular/core';
import { Observable, PartialObserver, Subscription } from 'rxjs';

@Component({
  template: ''
})
export abstract class SubscriptionManagementComponent implements OnDestroy {
  private subscriptionList: Subscription[] = [];

  protected addSubscription<T = any>(observable: Observable<T>, observer: PartialObserver<T>): void {
    const subscription = observable.subscribe(observer);

    this.subscriptionList.push(subscription);
  }

  public ngOnDestroy(): void {
    this.subscriptionList.forEach(subs => subs.unsubscribe());
  }
}
