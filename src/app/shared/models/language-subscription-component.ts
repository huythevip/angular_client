import { Component } from '@angular/core';
import { APP_LANGUAGES } from '../constants';
import { LanguageService } from '../services/general/language-service';
import texts from '../texts';
import { SubscriptionManagementComponent } from './subscription-management-component';

@Component({
  template: '',
})
export abstract class LanguageSubscriptionComponent extends SubscriptionManagementComponent {
  protected currentLanguage: APP_LANGUAGES;

  constructor(
    protected languageService: LanguageService,
  ) {
    super();

    this.listenForLanguageChange();
  }

  public get pageTexts(): any {
    return texts[this.currentLanguage];
  }

  protected listenForLanguageChange(): void {
    this.addSubscription(this.languageService.getLanguage$(), {
      next: lang => this.currentLanguage = lang,
    });
  }
}
