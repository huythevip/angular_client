import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormGroup } from '@angular/forms';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { PaginateUserPostsForm } from 'src/app/shared/forms/paginate-user-posts-form';
import { GeneralTypesDTO, UserHistoryPost } from 'src/app/shared/interfaces';
import { LanguageSubscriptionComponent } from 'src/app/shared/models/language-subscription-component';
import { AuthService } from 'src/app/shared/services/general/auth-service';
import { LanguageService } from 'src/app/shared/services/general/language-service';
import { NotificationService } from 'src/app/shared/services/notification/notification-service';
import { PetReadService } from 'src/app/shared/services/pets';
import { PostReadService } from 'src/app/shared/services/posts/post-read-service';
import { UserReadService } from 'src/app/shared/services/user/user-read-service';

@Component({
  selector: 'app-user-post-history',
  templateUrl: './user-post-history.component.html',
  styleUrls: ['./user-post-history.component.scss'],
  providers: [
    PaginateUserPostsForm,
  ],
})
export class UserHistoryPostComponent extends LanguageSubscriptionComponent implements OnInit {
  public userHistoryPosts: UserHistoryPost[] = [];
  public totalCount: number = 0;
  public isTableLoading: boolean = true;
  public isFilterFormLoading: boolean = true;

  public allPetTypes: GeneralTypesDTO[] = [];
  public allPostTypes: GeneralTypesDTO[] = [];

  protected confirmationModal: NzModalRef;

  constructor(
    languageService: LanguageService,
    protected userReadService: UserReadService,
    protected petReadService: PetReadService,
    protected postReadService: PostReadService,
    protected paginateUserHistoryPostsForm: PaginateUserPostsForm,
    protected authService: AuthService,
    protected notiService: NotificationService,
    protected modalService: NzModalService,
  ) {
    super(languageService);
  }

  public get paginatePostsForm(): FormGroup {
    return this.paginateUserHistoryPostsForm.formGroup;
  }

  public get isFilterDataReady(): boolean {
    return this.allPetTypes.length
      && this.allPostTypes.length
      && true;
  }

  public onConfirmDeletePost() {
    this.confirmationModal = this.modalService.confirm({
      nzTitle: this.pageTexts.DELETE_POST_CONFIRM_TITLE(),
      nzContent: this.pageTexts.DELETE_POST_CONFIRM_MESSAGE(),
      nzOnOk: this.handleDeletePost.bind(this),
    });
  }

  protected handleDeletePost(): Promise<void> {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        this.notiService.success(this.pageTexts.POST_DELETE_SUCCESS());
        this.userHistoryPosts = [];
        this.findUserPosts();
        resolve();
      }, 2000);
    });
  }

  protected findPetTypes(): void {
    this.isFilterFormLoading = true;
    this.petReadService.findPetTypes().subscribe({
      next: petTypes => this.allPetTypes = petTypes,
      complete: () => this.isFilterFormLoading = !this.isFilterDataReady,
    });
  }

  protected findPostTypes(): void {
    this.isFilterFormLoading = true;
    this.postReadService.findPostTypes().subscribe({
      next: postTypes => this.allPostTypes = postTypes,
      complete: () => this.isFilterFormLoading = !this.isFilterDataReady,
    })
  }

  public ngOnInit(): void {
    this.findUserPosts();
    this.findPetTypes();
    this.findPostTypes();
  }

  public submitForm(): void {
    this.findUserPosts();
  }

  protected findUserPosts(): void {
    const user = this.authService.currentUser;

    this.isTableLoading = true,
    this.userReadService.findUserPosts(user.id, this.paginateUserHistoryPostsForm.formValue).subscribe({
      next: paginateResult => {
        this.userHistoryPosts = paginateResult.docs;
        this.totalCount = paginateResult.totalCount;
      },
      error: () => {
        this.notiService.error(
          this.pageTexts.ERROR_FINDING(this.pageTexts.YOUR_HISTORY_POSTS()),
        )
      },
      complete: () => this.isTableLoading = false,
    });
  }

}
