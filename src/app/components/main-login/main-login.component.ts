import { Component } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginForm } from 'src/app/shared/forms/login-form';
import { LanguageSubscriptionComponent } from 'src/app/shared/models/language-subscription-component';
import { AuthService } from 'src/app/shared/services/general/auth-service';
import { LanguageService } from 'src/app/shared/services/general/language-service';
import { NotificationService } from 'src/app/shared/services/notification/notification-service';

@Component({
  selector: 'app-main-login',
  templateUrl: './main-login.component.html',
  styleUrls: ['./main-login.component.scss'],
  providers: [LoginForm],
})
export class MainLoginComponent extends LanguageSubscriptionComponent {
  constructor(
    protected loginFormService: LoginForm,
    protected authService: AuthService,
    protected notiService: NotificationService,
    protected router: Router,
    languageService: LanguageService,
  ) {
    super(languageService);
  }

  public get loginForm(): FormGroup {
    return this.loginFormService.formGroup;
  }

  public handleLogin(): void {
    if (this.loginForm.invalid) {
      this.notiService.error('Form is not valid');

      return;
    }

    this.addSubscription(
      this.authService.signIn(this.loginForm.value),
      {
        next: (user) => {
          this.notiService.success(this.languageService.renderScript('YOU_LOGGED_IN_SUCCESSFULLY'));
          this.router.navigate(['/my-account/users/profile/' + user.id]);
        },
        error: (error) => this.notiService.error(error),
      }
    );
  }
}
