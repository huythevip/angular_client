import { Component, OnInit } from '@angular/core';
import { LanguageSubscriptionComponent } from 'src/app/shared/models/language-subscription-component';
import { LanguageService } from 'src/app/shared/services/general/language-service';
import { TestimonialSliderController } from 'src/app/shared/services/image-slider-controllers/testimonial-slider-controller';

@Component({
  selector: 'app-home-body',
  templateUrl: './home-body.component.html',
  styleUrls: ['./home-body.component.scss'],
  providers: [TestimonialSliderController],
})
export class HomeBodyComponent extends LanguageSubscriptionComponent implements OnInit {
  public carouselImageSrc = [
    'assets/images/sample_pet_carousel_1.jpg',
    'assets/images/sample_pet_carousel_2.jpg',
    'assets/images/sample_pet_carousel_3.jpg',
  ];

  constructor(
    languageService: LanguageService,
    // protected testimonialSliderController: TestimonialSliderController,
  ) {
    super(languageService);
  }

  ngOnInit(): void {
  }

}
