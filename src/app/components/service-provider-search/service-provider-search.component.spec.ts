import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceProviderSearchComponent } from './service-provider-search.component';

describe('ServiceProviderSearchComponent', () => {
  let component: ServiceProviderSearchComponent;
  let fixture: ComponentFixture<ServiceProviderSearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ServiceProviderSearchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceProviderSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
