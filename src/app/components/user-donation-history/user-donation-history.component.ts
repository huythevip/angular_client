import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { UserDonationHistory } from 'src/app/shared/interfaces';
import { LanguageSubscriptionComponent } from 'src/app/shared/models/language-subscription-component';
import { HttpErrorHandlerPipe } from 'src/app/shared/pipes/http-error-handler-pipe';
import { DonationReadService } from 'src/app/shared/services/donation/donation-read-service';
import { AuthService } from 'src/app/shared/services/general/auth-service';
import { LanguageService } from 'src/app/shared/services/general/language-service';

@Component({
  selector: 'app-user-donation-history',
  templateUrl: './user-donation-history.component.html',
  styleUrls: ['./user-donation-history.component.scss']
})
export class UserDonationHistoryComponent extends LanguageSubscriptionComponent implements OnInit {
  public isTableLoading: boolean = false;

  public userDonationHistory: UserDonationHistory[] = [];

  constructor(
    protected languageService: LanguageService,
    protected donationReadService: DonationReadService,
    protected authService: AuthService,
    protected errorHandlerPipe: HttpErrorHandlerPipe,
  ) {
    super(languageService);
  }

  ngOnInit(): void {
    this.loadUserDonationHistory();
  }

  protected loadUserDonationHistory() {
    this.isTableLoading = true;
    const currentUser = this.authService.currentUser;

    this.donationReadService
      .findUserDonationHistory(currentUser.id)
      .pipe(catchError(this.errorHandlerPipe.defaultHandler()))
      .subscribe({
        next: data => this.userDonationHistory = data,
        complete: () => this.isTableLoading = false,
      })
  }
}
