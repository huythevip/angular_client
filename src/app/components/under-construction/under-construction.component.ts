import { Component, OnInit } from '@angular/core';
import { LanguageSubscriptionComponent } from 'src/app/shared/models/language-subscription-component';
import { LanguageService } from 'src/app/shared/services/general/language-service';

@Component({
  selector: 'app-under-construction',
  templateUrl: './under-construction.component.html',
  styleUrls: ['./under-construction.component.scss']
})
export class UnderConstructionComponent extends LanguageSubscriptionComponent implements OnInit {

  constructor(
    languageService: LanguageService
  ) { super(languageService); }

  ngOnInit(): void {
  }

}
