import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ImageSliderNavigationEvents, USER_TYPES } from 'src/app/shared/constants';
import { FindPetsDTO, GeneralTypesDTO, PetDetailDTO } from 'src/app/shared/interfaces';
import { LanguageSubscriptionComponent } from 'src/app/shared/models/language-subscription-component';
import { HttpErrorHandlerPipe } from 'src/app/shared/pipes/http-error-handler-pipe';
import { AuthService } from 'src/app/shared/services/general/auth-service';
import { LanguageService } from 'src/app/shared/services/general/language-service';
import { ImageSliderController } from 'src/app/shared/services/image-slider-controllers/base-slider-controller';
import { NotificationService } from 'src/app/shared/services/notification/notification-service';
import { PetReadService, PetWriteService } from 'src/app/shared/services/pets';

@Component({
  selector: 'app-home-pet-detail',
  templateUrl: './home-pet-detail.component.html',
  styleUrls: ['./home-pet-detail.component.scss']
})
export class HomePetDetailComponent extends LanguageSubscriptionComponent implements OnInit {
  public currentCarouselImgIndex: number = 0;
  public carouselImages: string[];
  public petData: PetDetailDTO;
  public isLoading: boolean = false;
  public carouselNavigation$: Observable<ImageSliderNavigationEvents>;

  protected carouselSliderController: ImageSliderController;

  protected allPetSpecialProperties = [];

  constructor(
    protected languageService: LanguageService,
    protected petReadService: PetReadService,
    protected petWriteService: PetWriteService,
    protected authService: AuthService,
    protected errorHandlerPipe: HttpErrorHandlerPipe,
    protected notiService: NotificationService,
    protected router: Router,
  ) {
    super(languageService);

    this.carouselSliderController = new ImageSliderController();
    this.carouselNavigation$ = this.carouselSliderController.navigation$;
  }

  ngOnInit(): void {
    this.listenForPetDetail();
    this.findPetHealthProperties();
  }

  public handleChangeImage(index: number): void {
    this.currentCarouselImgIndex = index;
  }

  public get petStatus(): string {
    return this.petData.isDone
      ? (this.petData.isAdopt ? this.pageTexts.ADOPTED() : this.pageTexts.FOUNDED())
      : (this.petData.isAdopt ? this.pageTexts.NOT_YET_ADOPTED() : this.pageTexts.NOT_YET_FOUND());
  }

  public getPetSpecialPropertyName(property: { id: number }): string {
    const { id } = property;
    const healthProperty = this.allPetSpecialProperties.find(i => i.id === id);

    return healthProperty && healthProperty.name || '';
  }

  public shouldDisplayActionBtn(): boolean {
    const currentUser = this.authService.currentUser;

    if (!currentUser) {
      return false;
    }

    const isAdmin = currentUser.userTypeId === USER_TYPES.ADMINISTRATOR;

    return isAdmin || ( this.petData.contact.id === currentUser.id );
  }

  public get actionBtnMessage(): string {
    if (this.petData.isDone) {
      return this.petData.isAdopt ? this.pageTexts.MARK_NOT_ADOPTED() : this.pageTexts.MARK_NOT_FOUND();
    }

    return this.petData.isAdopt ? this.pageTexts.MARK_ADOPTED() : this.pageTexts.MARK_FOUND();
  }

  public handleChangePetStatus(): void {
    let payload: any = {};
    
    this.isLoading = true;
    this.addSubscription(
      this.petWriteService
        .updatePetById(this.petData.id, payload)
        .pipe(catchError(this.errorHandlerPipe.defaultHandler())),
      {
        next: () => {
          this.notiService.success(this.pageTexts.OPERATION_SUCCESS());
          this.router.navigate(['/home/pets/find'])

        },
        complete: () => this.isLoading = false,
      },
    );
  }

  protected findPetHealthProperties(): void {
    this.isLoading = true;

    this.petReadService
      .findPetHealthProperties()
      .pipe(catchError(this.errorHandlerPipe.defaultHandler()))
      .subscribe({
        next: data => this.allPetSpecialProperties = data,
        complete: () => this.isLoading = false,
      });
  }

  protected listenForPetDetail(): void {
    this.addSubscription(
      this.petReadService.findPetDetailById(1),
      {
        next: petData => this.petData = petData,
      }
    );
  }
}
