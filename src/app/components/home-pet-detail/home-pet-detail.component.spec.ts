import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomePetDetailComponent } from './home-pet-detail.component';

describe('HomePetDetailComponent', () => {
  let component: HomePetDetailComponent;
  let fixture: ComponentFixture<HomePetDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomePetDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomePetDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
