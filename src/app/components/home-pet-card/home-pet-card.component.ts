import { Component, Input, OnInit } from '@angular/core';
import { FindPetsDTO } from 'src/app/shared/interfaces';
import { LanguageSubscriptionComponent } from 'src/app/shared/models/language-subscription-component';
import { LanguageService } from 'src/app/shared/services/general/language-service';

@Component({
  selector: 'app-home-pet-card',
  templateUrl: './home-pet-card.component.html',
  styleUrls: ['./home-pet-card.component.scss']
})
export class HomePetCardComponent extends LanguageSubscriptionComponent {
  @Input() pet: FindPetsDTO;

  constructor(
    languageService: LanguageService,
  ) {
    super(languageService);
  }

  ngOnInit(): void {
  }

}
