import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { PaginatePetsForm } from 'src/app/shared/forms/paginate-pets-form';
import { FindPetsDTO, GeneralTypesDTO, PaginateResult, PetBreedDTO } from 'src/app/shared/interfaces';
import { LanguageSubscriptionComponent } from 'src/app/shared/models/language-subscription-component';
import { HttpErrorHandlerPipe } from 'src/app/shared/pipes/http-error-handler-pipe';
import { LanguageService } from 'src/app/shared/services/general/language-service';
import { LocationReadService } from 'src/app/shared/services/location/location-read-service';
import { PetReadService } from 'src/app/shared/services/pets';

@Component({
  selector: 'app-find-pets',
  templateUrl: './find-pets.component.html',
  styleUrls: ['./find-pets.component.scss'],
  providers: [PaginatePetsForm],
})
export class FindPetsComponent extends LanguageSubscriptionComponent implements OnInit {
  public allLocations: GeneralTypesDTO[] = [];
  public allPetTypes: GeneralTypesDTO[] = [];
  public allSexTypes: GeneralTypesDTO[] = [];
  public allPetSizes: GeneralTypesDTO[] = [];
  public allPetBreeds: PetBreedDTO[] = [];
  public allPetAges: GeneralTypesDTO[] = [];
  public findPetsResult: FindPetsDTO[] = [];

  public totalCount: number = 0;
  public currentPage = 1;

  public additionalFilters: { propertyId: number, propertyValue: any }[] = [];

  public isFilterFormLoading: boolean = false;
  public isPetGridLoading: boolean = false;

  constructor(
    protected paginatePetsForm: PaginatePetsForm,
    protected petReadService: PetReadService,
    protected locationReadService: LocationReadService,
    protected httpErrorHandlerPipe: HttpErrorHandlerPipe,
    private route: ActivatedRoute,

    languageService: LanguageService,
  ) {
    super(languageService);
  }

  ngOnInit(): void {
    this.listenForRouteParamsChange();
    this.findLocations();
    this.findPetTypes();
    this.findSexTypes();
    this.findPets();
    this.findPetAges();
    this.findPetSizes();
    this.findPetBreeds();
  }

  public get paginatePetsFormGroup(): FormGroup {
    return this.paginatePetsForm.formGroup;
  }

  public addFilter(): void {
    this.additionalFilters.push({
      propertyId: null,
      propertyValue: null,
    });
  }

  public logSth($event, index: number): void {
    console.log($event);
  }

  public submitForm(): void {

  }

  public removeAdditionalFilter(index: number): void {
    this.additionalFilters.splice(index, 1);
  }

  public handlePageIndexChange(currentPage: number): void {
    console.log(currentPage);
  }

  public handleChangeFilterProperty(value: any, index: number): void {
    const chosenPropertyId = value;
    const previousPropertyValue = this.additionalFilters[index];

    previousPropertyValue.propertyId = chosenPropertyId;
  }

  public handleChangeFilterPropertyValue(value: any, index: number): void {
    const chosenPropertyValue = value;
    const previousPropertyValue = this.additionalFilters[index];

    previousPropertyValue.propertyValue.push(chosenPropertyValue);
  }

  protected get isFilterDataReady(): boolean {
    return this.allLocations.length
      && this.allPetTypes.length
      && this.allSexTypes.length
      && true;
  }

  protected listenForRouteParamsChange(): void {
    this.addSubscription(
      this.route.queryParams,
      {
        next: params => {
          this.paginatePetsForm
            .getFormControl('purpose')
            .setValue(params.purpose || 'findLost');
        },
      }
    )
  }

  protected findLocations(): void {
    this.locationReadService.findLocations()
      .pipe(catchError(this.httpErrorHandlerPipe.defaultHandler()))
      .subscribe({
        next: locations => this.allLocations = locations,
        complete: () => this.isFilterFormLoading = !this.isFilterDataReady,
      });
  }

  protected findPetTypes(): void {
    this.petReadService.findPetTypes()
      .pipe(catchError(this.httpErrorHandlerPipe.defaultHandler()))
      .subscribe({
        next: petTypes => this.allPetTypes = petTypes,
        complete: () => this.isFilterFormLoading = !this.isFilterDataReady,
      });
  }

  protected findPets(options?: any): void {
    this.isPetGridLoading = true;

    this.petReadService.findPets(options)
      .pipe(catchError(this.httpErrorHandlerPipe.defaultHandler()))
      .subscribe({
        next: (paginationData: PaginateResult<FindPetsDTO>) => {
          this.findPetsResult = paginationData.docs;
          this.totalCount = paginationData.totalCount;
        },
        complete: () => this.isPetGridLoading = false,
      });
  }

  protected findSexTypes(): void {
    this.petReadService.findPetSexes()
      .pipe(catchError(this.httpErrorHandlerPipe.defaultHandler()))
      .subscribe({
        next: data => this.allSexTypes = data,
        complete: () => this.isFilterFormLoading = !this.isFilterDataReady,
      });
  }

  protected findPetSizes(): void {
    this.petReadService.findPetSizes()
      .pipe(catchError(this.httpErrorHandlerPipe.defaultHandler()))
      .subscribe({
        next: data => this.allPetSizes = data,
        complete: () => this.isFilterFormLoading = !this.isFilterDataReady,
      });
  }

  protected findPetBreeds(): void {
    this.petReadService.findPetBreeds()
      .pipe(catchError(this.httpErrorHandlerPipe.defaultHandler()))
      .subscribe({
        next: data => this.allPetBreeds = data,
        complete: () => this.isFilterFormLoading = !this.isFilterDataReady,
      });
  }

  protected findPetAges(): void {
    this.petReadService.findPetAges()
      .pipe(catchError(this.httpErrorHandlerPipe.defaultHandler()))
      .subscribe({
        next: data => this.allPetAges = data,
        complete: () => this.isFilterFormLoading = !this.isFilterDataReady,
      });
  }
}
