import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { APP_DEVICES } from 'src/app/shared/constants';
import { CreatePetPostForm } from 'src/app/shared/forms/create-pet-post-form';
import { GeneralTypesDTO, PetBreedDTO } from 'src/app/shared/interfaces';
import { LanguageSubscriptionComponent } from 'src/app/shared/models/language-subscription-component';
import { HttpErrorHandlerPipe } from 'src/app/shared/pipes/http-error-handler-pipe';
import { DeviceService } from 'src/app/shared/services/general/device-service';
import { LanguageService } from 'src/app/shared/services/general/language-service';
import { NotificationService } from 'src/app/shared/services/notification/notification-service';
import { PetReadService } from 'src/app/shared/services/pets';
import { PostReadService } from 'src/app/shared/services/posts/post-read-service';

@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.scss'],
  providers: [CreatePetPostForm],
})
export class CreatePostComponent extends LanguageSubscriptionComponent implements OnInit {
  public currentFormStep = 0;
  public currentDevice: APP_DEVICES;
  public uploadUrl = 'https://www.mocky.io/v2/5cc8019d300000980a055e76';

  public petSpecialProperties: GeneralTypesDTO[] = [];

  public isUseUserAsContactPerson: boolean = true;
  public uploadFileList = [];

  public petAges: GeneralTypesDTO[] = [];
  public petSexes: GeneralTypesDTO[] = [];
  public petSizes: GeneralTypesDTO[] = [];
  public petTypes: GeneralTypesDTO[] = [];
  public petBreeds: PetBreedDTO[] = [];
  public postTypes: GeneralTypesDTO[] = [];

  constructor(
    languageService: LanguageService,
    protected deviceService: DeviceService,
    protected postReadService: PostReadService,
    protected createPetPostForm: CreatePetPostForm,
    protected petReadService: PetReadService,
    protected errorHandlerPipe: HttpErrorHandlerPipe,
    protected notificationService: NotificationService,
    protected router: Router,
  ) {
    super(languageService);
  }

  ngOnInit(): void {
    this.listenForDeviceChange();
    this.loadPetAges();
    this.loadPetBreeds();
    this.loadPetTypes();
    this.loadPetSizes();
    this.loadPostTypes();
    this.loadPetSexes();
  }

  public get createPostForm() {
    return this.createPetPostForm.formGroup;
  }

  public get canContinueUpload(): boolean {
    return this.uploadFileList.length < 2;
  }

  public get stepDirection(): string {
    return [APP_DEVICES.MOBILE, APP_DEVICES.TABLET].includes(this.currentDevice) ? 'vertical' : 'horizontal';
  }

  public goNextStep(): void {
    this.currentFormStep += 1;
  }

  public goPreviousStep(): void {
    this.currentFormStep -= 1;
  }

  public canGoNextStep(formGroupName: string): boolean {
    return this.createPetPostForm.getSubFormGroup(formGroupName).valid;
  }

  public handleUploadImage($event: any): void {
  }

  public handleFormSubmit(): void {
    this.notificationService.success(this.pageTexts.CREATE_POST_SUCCESS());
    this.router.navigate(['/my-account/users/posts']);
  }

  protected listenForDeviceChange(): void {
    this.addSubscription(
      this.deviceService.getDevice$(),
      {
        next: currentDevice => this.currentDevice = currentDevice,
      }
    )
  }

  protected loadPetBreeds(): void {
    this.petReadService.findPetBreeds()
      .pipe(catchError(this.errorHandlerPipe.defaultHandler()))
      .subscribe(data => this.petBreeds = data);
  }

  protected loadPostTypes(): void {
    this.postReadService.findPostTypes()
      .pipe(catchError(this.errorHandlerPipe.defaultHandler()))
      .subscribe(data => this.postTypes = data);
  }

  protected loadPetSizes(): void {
    this.petReadService.findPetSizes()
      .pipe(catchError(this.errorHandlerPipe.defaultHandler()))
      .subscribe(data => this.petSizes = data);
  }

  protected loadPetTypes(): void {
    this.petReadService.findPetTypes()
      .pipe(catchError(this.errorHandlerPipe.defaultHandler()))
      .subscribe(data => this.petTypes = data);
  }

  protected loadPetAges(): void {
    this.petReadService.findPetAges()
      .pipe(catchError(this.errorHandlerPipe.defaultHandler()))
      .subscribe(data => this.petAges = data);
  }

  protected loadPetSexes(): void {
    this.petReadService.findPetSexes()
      .pipe(catchError(this.errorHandlerPipe.defaultHandler()))
      .subscribe(data => this.petSexes = data);
  }
}
