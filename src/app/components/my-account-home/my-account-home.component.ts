import { Component, OnInit } from '@angular/core';
import { APP_DEVICES } from 'src/app/shared/constants';
import { LanguageSubscriptionComponent } from 'src/app/shared/models/language-subscription-component';
import { DeviceService } from 'src/app/shared/services/general/device-service';
import { LanguageService } from 'src/app/shared/services/general/language-service';

@Component({
  selector: 'app-my-account-home',
  templateUrl: './my-account-home.component.html',
  styleUrls: ['./my-account-home.component.scss']
})
export class MyAccountHomeComponent extends LanguageSubscriptionComponent implements OnInit {
  public isDrawerVisible = false;
  public currentDevice: APP_DEVICES;

  constructor(
    protected langageService: LanguageService,
    protected deviceService: DeviceService,
  ) {
    super(langageService);
  }

  ngOnInit(): void {
    this.listenForDeviceChange();
  }

  public toggleDrawer(): void {
    this.isDrawerVisible = !this.isDrawerVisible;
  }

  protected listenForDeviceChange(): void {
    this.addSubscription(
      this.deviceService.getDevice$(),
      {
        next: device => this.currentDevice = device,
      }
    );
  }

}
