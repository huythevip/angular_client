import { Component } from '@angular/core';
import { AbstractControl, FormGroup } from '@angular/forms';
import { RegisterForm } from 'src/app/shared/forms/register-form';
import { LanguageSubscriptionComponent } from 'src/app/shared/models/language-subscription-component';
import { LanguageService } from 'src/app/shared/services/general/language-service';

@Component({
  selector: 'app-main-register',
  templateUrl: './main-register.component.html',
  styleUrls: ['./main-register.component.scss'],
  providers: [RegisterForm],
})
export class MainRegisterComponent extends LanguageSubscriptionComponent {
  constructor(
    protected registerFormService: RegisterForm,
    languageService: LanguageService,
  ) {
    super(languageService);
  }

  public get loginForm(): FormGroup {
    return this.registerFormService.formGroup;
  }
}
