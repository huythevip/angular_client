import { Component } from '@angular/core';
import { AbstractControl, FormGroup } from '@angular/forms';
import { RegisterForm } from 'src/app/shared/forms/register-form';
import { LanguageSubscriptionComponent } from 'src/app/shared/models/language-subscription-component';
import { LanguageService } from 'src/app/shared/services/general/language-service';
import { NotificationService } from 'src/app/shared/services/notification/notification-service';

@Component({
  selector: 'app-register-modal',
  templateUrl: './register-modal.component.html',
  styleUrls: ['./register-modal.component.scss'],
  providers: [RegisterForm],
})
export class RegisterModalComponent extends LanguageSubscriptionComponent {

  constructor(
    protected registerFormService: RegisterForm,
    protected notificationService: NotificationService,
    languageService: LanguageService,
  ) {
    super(languageService);
  }

  public get registerForm(): FormGroup {
    return this.registerFormService.formGroup;
  }

  public get email(): AbstractControl {
    return this.registerFormService.getFormControl('email');
  }

  public get password(): AbstractControl {
    return this.registerFormService.getFormControl('password');
  }

  public get passwordConfirmation(): AbstractControl {
    return this.registerFormService.getFormControl('passwordConfirmation');
  }

  public get fullName(): AbstractControl {
    return this.registerFormService.getFormControl('fullName');
  }

  public handleSubmit(): void {
    this.addSubscription(
      this.registerFormService.submit(),
      {
        next: user => {
          this.notificationService.success('Registered successfully!');
        }
      }
    );
  }

}
