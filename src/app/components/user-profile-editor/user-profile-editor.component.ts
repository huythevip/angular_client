import { Component, ElementRef, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { catchError, concatMap, map } from 'rxjs/operators';
import { USER_TYPES } from 'src/app/shared/constants';
import { UserSecurityUpdateForm } from 'src/app/shared/forms/user-security-update-form';
import { UpdateUserProfileForm } from 'src/app/shared/forms/update-user-profile-form';
import { GeneralTypesDTO, IUser } from 'src/app/shared/interfaces';
import { LanguageSubscriptionComponent } from 'src/app/shared/models/language-subscription-component';
import { HttpErrorHandlerPipe } from 'src/app/shared/pipes/http-error-handler-pipe';
import { AuthService } from 'src/app/shared/services/general/auth-service';
import { LanguageService } from 'src/app/shared/services/general/language-service';
import { UploadService } from 'src/app/shared/services/general/upload-service';
import { NotificationService } from 'src/app/shared/services/notification/notification-service';
import { UserReadService } from 'src/app/shared/services/user/user-read-service';
import { UserWriteService } from 'src/app/shared/services/user/user-write-service';
import { ErrorTipMessageHelper } from 'src/app/shared/helpers/error-tip-message-helper';

@Component({
  selector: 'app-user-profile-editor',
  templateUrl: './user-profile-editor.component.html',
  styleUrls: ['./user-profile-editor.component.scss'],
  providers: [UpdateUserProfileForm, UserSecurityUpdateForm],
})
export class UserProfileEditorComponent extends LanguageSubscriptionComponent implements OnInit {
  public userProfile: IUser;
  public isUploadingImage = false;
  public userImageUrl: string;

  public userTypes: GeneralTypesDTO[] = [];

  protected formInputs: { [inputName: string]: { input: ElementRef<HTMLInputElement>, isDisabled: boolean } } = {
    email: null,
  };

  protected currentUser: IUser;

  protected formControlsLoadingStatus = {
    email: false,
    fullName: false,
    phoneNumber: false,
    address: false,
    accountTypeId: false,
    bio: false,
  };

  constructor(
    langaugeService: LanguageService,
    protected authService: AuthService,
    protected userReadService: UserReadService,
    protected userWriteService: UserWriteService,
    protected errorTipHelper: ErrorTipMessageHelper,
    protected route: ActivatedRoute,
    protected updateUserProfileForm: UpdateUserProfileForm,
    protected router: Router,
    protected errorHandlerPipe: HttpErrorHandlerPipe,
    protected notificationService: NotificationService,
    protected uploadService: UploadService,
    protected userSecurityUpdateForm: UserSecurityUpdateForm,
  ) {
    super(langaugeService);
  }

  ngOnInit(): void {
    this.listenForRouteParamsChange(this.loadUserProfile.bind(this));
    this.getUserTypes();
  }

  public get isLoadingProfile(): boolean {
    return this.userProfile ? false : true;
  }

  public get isUserAdmin(): boolean {
    return this.authService.currentUser.userTypeId === USER_TYPES.ADMINISTRATOR;
  }

  public get updateProfileForm() {
    return this.updateUserProfileForm.formGroup;
  }

  public get userSecurityForm() {
    return this.userSecurityUpdateForm.formGroup;
  }

  public toggleUpdateField(formControlName: string): void {
    this.updateUserProfileForm.toggleFormControlDisability(formControlName);
  }

  public handleChangePassword(): void {
    this.notificationService.success(this.pageTexts.OPERATION_SUCCESS());

    this.authService.signOut();
    this.router.navigate(["/home"]);
  }

  public getErrorMessageForFormControl(formControlName: string): string {
    const formControl = this.updateUserProfileForm.getFormControl(formControlName);

    return this.errorTipHelper.getErrorMessageForControl(formControl);
  }

  public isFormControlLoading(formControlName: string): boolean {
    return this.formControlsLoadingStatus[formControlName];
  }

  public isFormControlReadyUpdate(formControlName: string): boolean {
    return this.updateUserProfileForm.getFormControl(formControlName).enabled;
  }

  public handleUpdateGeneralInfo(formControlName: string): void {
    const userId = this.userProfile.id;
    const updateData = this.updateUserProfileForm.formValue;

    if (!this.updateUserProfileForm.formGroup.valid) {
      this.notificationService.error(this.pageTexts.FORM_IS_INVALID());

      return;
    }

    this.formControlsLoadingStatus[formControlName] = true;

    this.userWriteService
      .updateUserById(userId, updateData)
      .pipe(catchError(this.errorHandlerPipe.defaultHandler({ success: false, })))
      .subscribe({
        next: (response) => {
          if (response.success !== true) { return; }
          this.notificationService.success(this.pageTexts.OPERATION_SUCCESS());
          this.updateUserProfileForm.disableAllFormControls();
        },
        complete: () => {
          this.formControlsLoadingStatus[formControlName] = false;
        },
      })
  }

  public handleFileChange($event: any) {
    const file: File = $event.target.files[0];

    this.isUploadingImage = true;
    this.uploadService
      .getUploadUrl()
      .pipe(
        concatMap(result => {
          const { uploadUrl, isSuccess, message } = result;
          if (!isSuccess) { throw new Error(message) }

          return this.uploadService.upload(uploadUrl, file);
        }),
      )
      .subscribe({
        next: (response) => {
          this.notificationService.success(this.pageTexts.UPLOAD_SUCCESS_MESSAGE());
          this.isUploadingImage = false;
          this.userImageUrl = response.fileUrl;
        },
        error: () => {
          this.notificationService.error(this.pageTexts.DEFAULT_ERROR_MESSAGE());
          this.isUploadingImage = false;
        },
      })
  }

  protected resetFormStatus(): void {
    this.formControlsLoadingStatus = {
      email: false,
      fullName: false,
      phoneNumber: false,
      address: false,
      accountTypeId: false,
      bio: false,
    };
  }

  protected listenForRouteParamsChange(onRouteChangeCallback: any): void {
    this.addSubscription(
      this.route.params,
      {
        next: params => {
          const userId = params.userId;
          const parseduserId = Number(userId);

          if (!userId) {
            this.notificationService.error(this.pageTexts.MISSING_POST_ID());
            this.router.navigate(['/my-account/users/posts']);

            return;
          }

          if (isNaN(userId) || parseduserId === 0) {
            this.notificationService.error(this.pageTexts.INVALID_POST_ID());
            this.router.navigate(['/my-account/users/posts']);

            return;
          }

          onRouteChangeCallback(userId);
        },
      }
    )
  }

  protected loadUserProfile(userId: number): void {
    this.userReadService.findUserById(userId)
      .pipe(catchError(this.errorHandlerPipe.defaultHandler()))
      .subscribe({
        next: (userProfile: IUser) => {
          this.userProfile = userProfile;

          const { id, ...profileData } = userProfile;
          this.updateUserProfileForm.formGroup.setValue(profileData);
          this.updateUserProfileForm.disableAllFormControls();

          const userTypeId = userProfile.userTypeId;
          this.userSecurityUpdateForm.formGroup.setValue({ 
            userTypeId,
            currentPassword: '',
            newPassword: '',
            confirmPassword: '',
          });

          if (userTypeId !== USER_TYPES.ADMINISTRATOR) {
            this.userSecurityUpdateForm.disableFormControl('userTypeId');
          }
        },
      });
  }

  protected getUserTypes(): void {
    this.userReadService.findUserTypes()
      .subscribe({
        next: data => this.userTypes = data,
      });
  }
}
