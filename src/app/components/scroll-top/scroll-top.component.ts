import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-scroll-top',
  templateUrl: './scroll-top.component.html',
  styleUrls: ['./scroll-top.component.scss']
})
export class ScrollTopComponent implements OnInit {
  @Input() isDisplayScollTopButton: boolean = true;
  @Input() heightToStartScroll: number = 50;
  

  ngOnInit(): void {
    this.scrollTop();
  }

  protected scrollTop(): void {
    if (
        document.body.scrollTop > this.heightToStartScroll
     || document.documentElement.scrollTop > this.heightToStartScroll
    ) {
      document.body.scrollTop = 0; // For Safari
      document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
    }
  }

}
