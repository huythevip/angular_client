import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { APP_DEVICES } from 'src/app/shared/constants';
import { UpdatePetPostForm } from 'src/app/shared/forms/update-pet-post-form';
import { GeneralTypesDTO, PetBreedDTO, UserHistoryPost } from 'src/app/shared/interfaces';
import { LanguageSubscriptionComponent } from 'src/app/shared/models/language-subscription-component';
import { HttpErrorHandlerPipe } from 'src/app/shared/pipes/http-error-handler-pipe';
import { AuthService } from 'src/app/shared/services/general/auth-service';
import { DeviceService } from 'src/app/shared/services/general/device-service';
import { LanguageService } from 'src/app/shared/services/general/language-service';
import { NotificationService } from 'src/app/shared/services/notification/notification-service';
import { PetReadService } from 'src/app/shared/services/pets';
import { PostReadService } from 'src/app/shared/services/posts/post-read-service';
import { UserReadService } from 'src/app/shared/services/user/user-read-service';

@Component({
  selector: 'app-update-post',
  templateUrl: './update-post.component.html',
  styleUrls: ['./update-post.component.scss'],
  providers: [UpdatePetPostForm],
})
export class UpdatePostComponent extends LanguageSubscriptionComponent implements OnInit {
  public currentFormStep = 0;
  public post: UserHistoryPost;
  public currentDevice: APP_DEVICES;
  public uploadUrl = 'https://www.mocky.io/v2/5cc8019d300000980a055e76';

  public petSpecialProperties: GeneralTypesDTO[] = [];

  public isUseUserAsContactPerson: boolean = true;
  public uploadFileList = [];

  public petAges: GeneralTypesDTO[] = [];
  public petSexes: GeneralTypesDTO[] = [];
  public petSizes: GeneralTypesDTO[] = [];
  public petTypes: GeneralTypesDTO[] = [];
  public petBreeds: PetBreedDTO[] = [];
  public postTypes: GeneralTypesDTO[] = [];

  constructor(
    languageService: LanguageService,
    protected deviceService: DeviceService,
    protected updatePetPostForm: UpdatePetPostForm,
    protected postReadService: PostReadService,
    protected petReadService: PetReadService,
    protected userReadService: UserReadService,
    protected errorHandlerPipe: HttpErrorHandlerPipe,
    protected notificationService: NotificationService,
    protected authService: AuthService,
    protected router: Router,
    protected route: ActivatedRoute,
  ) {
    super(languageService);
  }

  ngOnInit(): void {
    this.listenForRouteParamsChange();
    this.listenForDeviceChange();
    this.loadPetAges();
    this.loadPetBreeds();
    this.loadPetTypes();
    this.loadPetSexes();
    this.loadPetSizes();
    this.loadPostTypes();
  }

  public get isLoadingDataForUpdate(): boolean {
    return (this.petAges.length
      && this.petBreeds.length
      && this.petTypes.length
      && this.petSexes.length
      && this.postTypes.length
      && this.petSizes.length
      && this.post)
      ? false : true;
  }

  public get updatePostForm() {
    return this.updatePetPostForm.formGroup;
  }

  public get canContinueUpload(): boolean {
    return this.uploadFileList.length < 2;
  }

  public get stepDirection(): string {
    return [APP_DEVICES.MOBILE, APP_DEVICES.TABLET].includes(this.currentDevice) ? 'vertical' : 'horizontal';
  }

  public goNextStep(): void {
    this.currentFormStep += 1;
  }

  public goPreviousStep(): void {
    this.currentFormStep -= 1;
  }

  public canGoNextStep(formGroupName: string): boolean {
    return this.updatePetPostForm.getSubFormGroup(formGroupName).valid;
  }

  public handleUploadImage($event: any): void {
  }

  public handleFormSubmit(): void {
    this.notificationService.success(this.pageTexts.UPDATE_POST_SUCCESS());
    this.router.navigate(['/my-account/users/posts']);
  }

  protected listenForRouteParamsChange(): void {
    this.addSubscription(
      this.route.params,
      {
        next: params => {
          const postId = params.postId;
          const parsedPostId = Number(postId);

          if (!postId) {
            this.notificationService.error(this.pageTexts.MISSING_POST_ID());
            this.router.navigate(['/my-account/users/posts']);

            return;
          }

          if (isNaN(postId) || parsedPostId === 0) {
            this.notificationService.error(this.pageTexts.INVALID_POST_ID());
            this.router.navigate(['/my-account/users/posts']);

            return;
          }

          this.loadUserPost(postId);
        },
      }
    )
  }

  protected loadUserPost(postId: number): void {
    const currentUser = this.authService.currentUser;

    this.userReadService.findUserPostById(currentUser.id, postId)
      .pipe(catchError(this.errorHandlerPipe.defaultHandler()))
      .subscribe({
        next: post => {
          this.post = post;
          const { id, createdAt, ...formData } = post;
          this.updatePetPostForm.formGroup.setValue(formData);
        },
      });
  }

  protected listenForDeviceChange(): void {
    this.addSubscription(
      this.deviceService.getDevice$(),
      {
        next: currentDevice => this.currentDevice = currentDevice,
      }
    )
  }

  protected loadPetBreeds(): void {
    this.petReadService.findPetBreeds()
      .pipe(catchError(this.errorHandlerPipe.defaultHandler()))
      .subscribe(data => this.petBreeds = data);
  }

  protected loadPetSizes(): void {
    this.petReadService.findPetSizes()
      .pipe(catchError(this.errorHandlerPipe.defaultHandler()))
      .subscribe(data => this.petSizes = data);
  }

  protected loadPetTypes(): void {
    this.petReadService.findPetTypes()
      .pipe(catchError(this.errorHandlerPipe.defaultHandler()))
      .subscribe(data => this.petTypes = data);
  }

  protected loadPostTypes(): void {
    this.postReadService.findPostTypes()
      .pipe(catchError(this.errorHandlerPipe.defaultHandler()))
      .subscribe(data => this.postTypes = data);
  }

  protected loadPetAges(): void {
    this.petReadService.findPetAges()
      .pipe(catchError(this.errorHandlerPipe.defaultHandler()))
      .subscribe(data => this.petAges = data);
  }

  protected loadPetSexes(): void {
    this.petReadService.findPetSexes()
      .pipe(catchError(this.errorHandlerPipe.defaultHandler()))
      .subscribe(data => this.petSexes = data);
  }
}
