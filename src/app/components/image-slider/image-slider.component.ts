import { AfterContentInit, Component, ElementRef, Input, QueryList, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { ImageSliderMode, ImageSliderNavigationEvents } from 'src/app/shared/constants';
import { LanguageSubscriptionComponent } from 'src/app/shared/models/language-subscription-component';
import { LanguageService } from 'src/app/shared/services/general/language-service';

@Component({
  selector: 'app-image-slider',
  templateUrl: './image-slider.component.html',
  styleUrls: ['./image-slider.component.scss']
})
export class ImageSliderComponent extends LanguageSubscriptionComponent implements AfterContentInit {
  @ViewChild('imageSlider') imageSlider: ElementRef;

  @Input() slideMode: ImageSliderMode = ImageSliderMode.CLICK;
  @Input() navigation$: Observable<ImageSliderNavigationEvents>;

  constructor(
    languageService: LanguageService,
  ) {
    super(languageService);
  }

  public ngAfterContentInit(): void {
    this.listenForNavigationEvent();
  }

  protected listenForNavigationEvent(): void {
    if (!this.navigation$) {
      return;
    }

    this.addSubscription(
      this.navigation$,
      {
        next: event => this.handleNavigationEvent(event),
      }
    );
  }

  protected handleNavigationEvent(event: ImageSliderNavigationEvents): void {
    // const currentScrollValue = this.imageSlider.nativeElement.scrollLeft;

    switch (event) {
      case ImageSliderNavigationEvents.FORWARD:
        this.imageSlider.nativeElement.scrollLeft += 200;
        break;
      case ImageSliderNavigationEvents.BACK:
        this.imageSlider.nativeElement.scrollLeft -= 200;
        break;
    }
  }
}
