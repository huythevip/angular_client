import { Component, OnInit } from '@angular/core';
import { LanguageSubscriptionComponent } from 'src/app/shared/models/language-subscription-component';
import { LanguageService } from 'src/app/shared/services/general/language-service';

@Component({
  selector: 'app-create-project',
  templateUrl: './create-project.component.html',
  styleUrls: ['./create-project.component.scss']
})
export class CreateProjectComponent extends LanguageSubscriptionComponent implements OnInit {
  public currentFormStep = 0;

  constructor(
    languageService: LanguageService,
  ) {
    super(languageService);
  }

  ngOnInit(): void {
  }

  public handleGoNextStep(): void {
    this.currentFormStep++;
  }

  public handleGoPreviousStep(): void {
    this.currentFormStep--;
  }

}
