import { Component, OnInit } from '@angular/core';
import { APP_DEVICES, APP_LANGUAGES, USER_TYPES } from 'src/app/shared/constants';
import { LanguageSubscriptionComponent } from 'src/app/shared/models/language-subscription-component';
import { LanguageService } from 'src/app/shared/services/general/language-service';
import { HomeHeaderInputService } from './home-header-input.service';
import { HomeHeaderOutputService } from './home-header-output.service';
import { NotificationService } from 'src/app/shared/services/notification/notification-service';
import { IUser } from 'src/app/shared/interfaces';

@Component({
  selector: 'app-home-header',
  templateUrl: './home-header.component.html',
  providers: [
    HomeHeaderInputService,
    HomeHeaderOutputService,
  ],
  styleUrls: ['./home-header.component.scss']
})
export class HomeHeaderComponent extends LanguageSubscriptionComponent implements OnInit {
  public user: IUser;
  public currentDevice: APP_DEVICES;
  public isDrawerVisible = false;

  protected currentLanguage: APP_LANGUAGES;

  constructor(
    private inputService: HomeHeaderInputService,
    private outputService: HomeHeaderOutputService,
    private notificationService: NotificationService,
    languageService: LanguageService,
  ) {
    super(languageService);
  }

  ngOnInit(): void {
    this.listenForUserChange();
    this.listenForDeviceChange();
  }

  public handleChangeLanguage(lang: string): void {
    return this.outputService.handleChangeLanguageOutput(lang as APP_LANGUAGES);
  }

  public toggleDrawer(): void {
    this.isDrawerVisible = !this.isDrawerVisible;
  }

  public handleSignOut(): void {
    this.outputService.handleSignOutOutput();
  }

  public handleSignIn(): void {
    this.notificationService.success(
      this.languageService.renderScript('YOU_LOGGED_IN_SUCCESSFULLY')
    );
    this.outputService.handleSignInOutput();
  }

  public get userTitle(): string {
    if (!this.user) {
      return;
    }

    switch (this.user.userTypeId) {
      case USER_TYPES.ADMINISTRATOR:
        return this.pageTexts.ADMINISTRATOR();
      case USER_TYPES.MODERATOR:
        return this.pageTexts.MODERATOR();
      case USER_TYPES.USER:
        return this.pageTexts.USER();
    }
  }

  protected listenForUserChange(): void {
    this.addSubscription<IUser>(this.inputService.getUser$(), {
      next: user => this.user = user,
    });
  }

  protected listenForDeviceChange(): void {
    this.addSubscription(this.inputService.getCurrentDevice$(), {
      next: device => this.currentDevice = device,
    });
  }
}
