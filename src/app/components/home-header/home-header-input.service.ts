import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { APP_DEVICES, APP_LANGUAGES } from 'src/app/shared/constants';
import { IUser } from 'src/app/shared/interfaces';
import { AuthService } from 'src/app/shared/services/general/auth-service';
import { DeviceService } from 'src/app/shared/services/general/device-service';
import { LanguageService } from 'src/app/shared/services/general/language-service';

@Injectable()
export class HomeHeaderInputService {
  protected user$: Observable<IUser>;

  constructor(
    protected authService: AuthService,
    protected languageService: LanguageService,
    protected devideService: DeviceService,
  ) {
    this.user$ = authService.getUser$();
  }

  public getUser$(): Observable<IUser> {
    return this.user$;
  }

  public getCurrentDevice$(): Observable<APP_DEVICES> {
    return this.devideService.getDevice$();
  }

  public getCurrentLang$(): Observable<APP_LANGUAGES> {
    return this.languageService.getLanguage$();
  }
}
