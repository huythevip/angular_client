import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { APP_LANGUAGES } from 'src/app/shared/constants';
import { IUser } from 'src/app/shared/interfaces';
import { AuthService } from 'src/app/shared/services/general/auth-service';
import { LanguageService } from 'src/app/shared/services/general/language-service';

@Injectable()
export class HomeHeaderOutputService {
  protected user$: Observable<IUser>;

  constructor(
    protected authService: AuthService,
    protected languageService: LanguageService,
  ) {
    this.user$ = authService.getUser$();
  }

  public handleSignOutOutput(): void {
    return this.authService.signOut();
  }

  public handleSignInOutput(): void {
    return this.authService.loadSampleUser();
  }

  public handleChangeLanguageOutput(lang: APP_LANGUAGES): void {
    return this.languageService.setCurrentLanguage(lang);
  }
}
